#!/usr/bin/env python


'''     @package LabelVid3D
        pose_array from the package LabelVid3D - 
          
        For maintaining an array of poses - that is tracking or label data for a video

        --------------------------------------------------------------------
        
        Copyright (C) 2013  Damien Jade Duff (Delfina's Dad)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
            
'''        
import transformations
import sys
import copy
import math

try:
  import numpy
  import numpy.linalg
except ImportError:
  print "Numpy must be installed to run this."         
  sys.exit(1)
  

try:
  from OpenGL import GL
  from OpenGL import GLU
except ImportError:
  print >> sys.stderr , "WARNING: GL support not available for pose_array module"


  
def debug_trace():
  '''Set a tracepoint in the Python debugger that works with Qt'''
  from PyQt4.QtCore import pyqtRemoveInputHook
  from pdb import set_trace
  pyqtRemoveInputHook()
  set_trace()    

class EulerAngles:
  def __init__(self):
    self.phi=None
    self.theta=None
    self.psi=None
  def __repr__(self):
    return  "EulerAngle(phi=%f,the=%f,psi=%f)" %(self.phi,self.theta,self.psi)
    
    

def quaternionToAxis(q,a):
  qw=q.qw
  qx=q.qx
  qy=q.qy
  qz=q.qz

  vecpart=numpy.array([qx,qy,qz])
  
  angle=2*math.atan2(numpy.linalg.norm(vecpart),qw)

  if angle==0:
    return [0,0,0]
  axis=vecpart/math.sin(angle/2)
  a2=angle*axis
  a[0]=a2[0]
  a[1]=a2[1]
  a[2]=a2[2]
  return
  

def axisToQuaternion(a,q):
  #print "axisToQuaternion(",a,",",q,")"
  angle=numpy.linalg.norm(numpy.array(a))
  if angle<>0:
    axis=a/angle
  else:
    axis=numpy.array([0,0,0])

  #print "axisToQuaternion(",a,",",q,")"
  
  vecpart=axis*math.sin(angle/2)
  
  q.qx=vecpart[0]
  q.qy=vecpart[1]
  q.qz=vecpart[2]
  
  q.qw=math.cos(angle/2)
  #q.qw=math.sqrt(1-q.qx**2-q.qy**2-q.qz**2)

  #print "       ",q
  return

  
  

'''This object is immutable as it makes it easier to deal with interpolation etc'''
class Pose:
  def __init__(self,x,y,z,qx,qy,qz,qw):
    self.x=x
    self.y=y
    self.z=z
    self.qx=qx
    self.qy=qy
    self.qz=qz
    self.qw=qw
   
  def changeX(self,x):
    newpose=copy.copy(self)
    newpose.x=x
    return newpose
    
  def changeY(self,y):
    newpose=copy.copy(self)
    newpose.y=y
    return newpose
    
  def changeZ(self,z):
    newpose=copy.copy(self)
    newpose.z=z
    return newpose   
      
  def __repr__(self):
    return  "Pose(%f,%f,%f,%f,%f,%f,%f)" %(self.x,self.y,self.z,self.qx,self.qy,self.qz,self.qw)
    
  def magnitudeQuat(self):
    squaredsize = self.qw * self.qw + self.qx * self.qx + self.qy * self.qy + self.qz * self.qz;
    return math.sqrt(squaredsize)
    
  def normalizeQuat(self):
    
    normtol=0.00001
    squaredsize = self.qw * self.qw + self.qx * self.qx + self.qy * self.qy + self.qz * self.qz;
    
    if abs(squaredsize -1) > normtol:
      magnitude=math.sqrt(squaredsize)
      if squaredsize < normtol:
        print "BAD MAGNITUDE:"
        
        debug_trace()
      newpose=copy.copy(self)
      newpose.qx=self.qx/magnitude
      newpose.qy=self.qy/magnitude
      newpose.qz=self.qz/magnitude
      newpose.qw=self.qw/magnitude
      return newpose
    else:
      return self
      
  def rotAsEuler(self,order):
    e = EulerAngles()
    e2=transformations.euler_from_quaternion([self.qx,self.qy,self.qz,self.qw],order)
    e.phi=e2[0]
    e.theta=e2[1]
    e.psi=e2[2]
    return e

    
  def rotFromEuler(self,e,order):
    newpose=copy.copy(self)
    quat=transformations.quaternion_from_euler(e.phi, e.theta, e.psi,order)
    newpose.qx=quat[0]
    newpose.qy=quat[1]
    newpose.qz=quat[2]
    newpose.qw=quat[3]
    return newpose   

  def rotAsAxis(self):
    a = numpy.zeros(3)
    quaternionToAxis(self,a)
    return a
      
  def rotFromAxis(self,a):
    
    axisToQuaternion(a,newpose)
    return newpose   

  def transformPoint(self,pt):
    x=pt[0]
    y=pt[1]
    z=pt[2]
    quat=[self.qx,self.qy,self.qz,self.qw]
    quatc=transformations.quaternion_conjugate(quat)
    rotted=transformations.quaternion_multiply(transformations.quaternion_multiply(quat,[x,y,z,0]),quatc)
    
    x=rotted[0]+self.x
    y=rotted[1]+self.y
    z=rotted[2]+self.z
    
    return [x,y,z]
    
  def mult_quat(self,mult):
    #print "mult_quat(",self,",",mult,")"
    newpose=copy.copy(self)
    newquat=transformations.quaternion_multiply([mult.qx,mult.qy,mult.qz,mult.qw],[self.qx,self.qy,self.qz,self.qw])
    newpose.qx=newquat[0]
    newpose.qy=newquat[1]
    newpose.qz=newquat[2]
    newpose.qw=newquat[3]
    return newpose
    
    
  def only_rot(self):
    newpose=copy.copy(self)
    newpose.x=0
    newpose.y=0
    newpose.z=0
    return newpose
    
  def invert(self):
    invrot=transformations.quaternion_inverse([self.qx,self.qy,self.qz,self.qw])
    newpose=Pose(0,0,0,invrot[0],invrot[1],invrot[2],invrot[3])
    newtrans=newpose.transformPoint([self.x,self.y,self.z])
    newpose.x=-newtrans[0]
    newpose.y=-newtrans[1]
    newpose.z=-newtrans[2]
    return newpose
    
    
    
  def mult_axis(self,axis):
    #print "mult_axis(",self,",",axis,")"
    quatrot=Pose(0,0,0,0,0,0,1)
    #debug_trace()
    
    axisToQuaternion(axis,quatrot)
    return self.mult_quat(quatrot)
      
  def normalizeQuatW(self):
    #normtol=0.00001
    normtol=0
    squaredsize = self.qw * self.qw + self.qx * self.qx + self.qy * self.qy + self.qz * self.qz;
    
    if abs(squaredsize -1) > normtol:
      
      if squaredsize < normtol:
        print "BAD MAGNITUDE:"
        #should not happen if normtol is 0
        debug_trace()
      
      newpose=copy.copy(self)
      
      newqw_squared=1 + self.qw**2 -squaredsize
      if newqw_squared>=0:
        newpose.qw=math.sqrt(1 + self.qw**2 -squaredsize)
        if self.qw<0: newpose.qw=-newpose.qw # keep the sign correct otherwise you can never rotate beyond 180
        return newpose
      else:
        newpose.qw=0
        return newpose.normalizeQuat() # if we cannot normalise by fixing qw, we will normalise by proportionally reducing the vector part (with scalar part already 0)
    else:
      return self
      
      
  def csvString(self):
    return  "%f,%f,%f,%f,%f,%f,%f" %(self.x,self.y,self.z,self.qx,self.qy,self.qz,self.qw)

  
    
  
      ##use quaternion to rotation matrix
  def getGLMultMatrix(self):
    qx2 = self.qx * self.qx
    qy2 = self.qy * self.qy
    qz2 = self.qz * self.qz
    qxy = self.qx * self.qy
    qxz = self.qx * self.qz
    qyz = self.qy * self.qz
    qwx = self.qw * self.qx
    qwy = self.qw * self.qy
    qwz = self.qw * self.qz
    
    #m=numpy.zeros((16))
    m=[0.0 for a in range(0,16)]
     
        
    m[0]=1.0 - 2.0 * (qy2 + qz2);
    m[1]=2.0 * (qxy - qwz);
    m[2]=2.0 * (qxz + qwy);
    m[3]=0.0;
    m[4]=2.0 * (qxy + qwz);
    m[5]=1.0 - 2.0 * (qx2 + qz2);
    m[6]=2.0 * (qyz - qwx);
    m[7]=0.0;
    m[8]=2.0 * (qxz - qwy);
    m[9]=2.0 * (qyz + qwx);
    m[10]=1.0 - 2.0 * (qx2 + qy2);
    m[11]=0.0;
    m[12]=self.x#0.0;
    m[13]=self.y#0.0;
    m[14]=self.z#0.0;
    m[15]=1.0;
    return m
    
  def multGL(self):
    m=self.getGLMultMatrix()
    GL.glMultMatrixd(m)

    
class InterpolatingPoseArray:
  def __init__(self,size=None):
    if size is not None:
      self.size=size
      self.changingsize=False
    else:
      self.size=0
      self.changingsize=True
    self.known_frames=dict()
    
    
  def normalizeAllQuats(self):
    for key,item in self.known_frames.items():
      self.known_frames[key]=item.normalizeQuat()
      #self.known_frames[key]=item.normalizeQuatW()
    
  def __getitem__(self,id):
    if id<0: raise IndexError
    
    if id>=self.size: 
      #if self.changingsize:
        #self.size=id+1
      #else:
      raise IndexError
    if id in self.known_frames.keys():
      #print "poses["+str(id)+"]-->"+str(self.known_frames[id])+"(known)"
      frame=self.known_frames[id]
      #print "For id ",id," returning ",frame
      #frame.status="Labelled"
      return frame
    else:

      idlow=id
      idhigh=id
      while idlow>=0 and not idlow in self.known_frames.keys():
        idlow=idlow-1
      while idhigh<self.size and not idhigh in self.known_frames.keys():
        idhigh=idhigh+1        
      #print "initial idlow="+str(idlow)
      #print "initial idhigh="+str(idhigh)
      if idlow<0 and idhigh>=self.size:
        raise IndexError
      elif idlow<0:
        highpose=self.known_frames[idhigh]
        lowpose=self.known_frames[idhigh]
        idlow=0
      elif idhigh>=self.size:
        highpose=self.known_frames[idlow]
        lowpose=self.known_frames[idlow]
        idhigh=self.size-1
      else:
        highpose=self.known_frames[idhigh]
        lowpose=self.known_frames[idlow]
      #print "final idlow="+str(idlow)
      #print "final idhigh="+str(idhigh)   
      
      #print "Interpolating",idlow," and ",idhigh," to get ",id," this is between poses ",lowpose, " and ",highpose
      
      framediffhigh=idhigh-id
      framedifflow=id-idlow
      #print "framediffhigh="+str(framediffhigh)
      #print "framedifflow="+str(framedifflow)
      #print "lowpose="+str(lowpose)
      #print "highpose="+str(highpose)
      totalframediff=framediffhigh+framedifflow
      
      x=(highpose.x*framedifflow+lowpose.x*framediffhigh)/totalframediff
      y=(highpose.y*framedifflow+lowpose.y*framediffhigh)/totalframediff
      z=(highpose.z*framedifflow+lowpose.z*framediffhigh)/totalframediff
      
      if False:
        nq=transformations.quaternion_slerp([lowpose.qx,lowpose.qy,lowpose.qz,lowpose.qw],[highpose.qx,highpose.qy,highpose.qz,highpose.qw],float(framedifflow)/float(totalframediff))
        newpose=Pose(x,y,z,nq[0],nq[1],nq[2],nq[3])
      if True:
        qx=(highpose.qx*framedifflow+lowpose.qx*framediffhigh)/totalframediff
        qy=(highpose.qy*framedifflow+lowpose.qy*framediffhigh)/totalframediff
        qz=(highpose.qz*framedifflow+lowpose.qz*framediffhigh)/totalframediff
        qw=(highpose.qw*framedifflow+lowpose.qw*framediffhigh)/totalframediff
        newpose=Pose(x,y,z,qx,qy,qz,qw)
        
      newpose=newpose.normalizeQuat()

      return newpose      
    
    
  def labelledFrames(self):
    a=[int(x) for x in self.known_frames.keys()]
    a.sort()
    return a
  
  def frameStatus(self,id):
    if id in self.known_frames.keys():
      return "Labelled"
    else:
      return "Interpolated"
  
  def __setitem__(self,id,pose):
    #print "poses["+str(id)+"]<--"+str(pose)
    if id<0: raise IndexError
    if id>=self.size: 
      if self.changingsize:
        self.size=id+1
      else:
        raise IndexError

    if pose is None:
      if id in self.known_frames:
        if len(self.known_frames) <= 1:
          QtGui.QMessageBox.warning(self,"Could not delete frame - need at least one labelled frame.")
        else:
          del self.known_frames[id]
    else:
      #normalizeQuatW
      self.known_frames[id]=pose.normalizeQuat()      #
      #self.known_frames[id]=pose.normalizeQuatW()      #
      #self.known_frames[id]=pose      
    
  def toSaveString(self,perframe):
    s="3D Monocularly Labelled Data Version 1\n"
    s+="TITLE,MODELNO,FRAMENO,TIME,X,Y,Z,QX,QY,QZ,QW,SOURCE\n"
    for ind in range(0,self.size):
      p=self[ind]
      s+="TIMEPOINT,0,"+str(ind)+","+str(perframe*ind)+","+p.csvString()+","+self.frameStatus(ind)+"\n"
    return s
    
  def fromFile(self,filename):
    f=open(filename,'r')
    first_line=f.readline().strip()
    
    if first_line=="MorTrack Tracking Output Version 1":
      print "Reading file from original tracker output filetype"
      self.readV0(f)
    elif first_line=="3D Monocularly Labelled Data Version 1":
      print "Reading file from this program's V1 output"
      self.readV1(f)
    else:
      print >> sys.stderr , "Could not read any labelled data from file ",filename
      print "Could not read any labelled data from file ",filename
      print "The first line is funny: ",first_line     
      print "Continuing anyway.."
    self.normalizeAllQuats()#some data comes to us with unnormalised quaternions - HWO COULD THIS HAVE HAPPENED??? ;)
      
  def readV0(self,f):
    header=f.readline().strip()
    assert(header=="TITLE,MODELNO,FRAMENO,TIME,X,Y,Z,QX,QY,QZ,QW")
    line=f.readline()
    self.known_frames.clear()
    while line:
      #if line[-1]=="\n":
        #line=line[:-1]
      fields=line.strip().split(",")
      #fields=re.split("[A-Z,a-z,0-9,.]+",line)
      if fields[0]=="TIMEPOINT":
        frameno=int(fields[2])
        x=float(fields[4])
        y=float(fields[5])
        z=float(fields[6])
        qx=float(fields[7])
        qy=float(fields[8])
        qz=float(fields[9])
        qw=float(fields[10])
        self.known_frames[frameno]=Pose(x,y,z,qx,qy,qz,qw)
        if self.changingsize and frameno>=self.size:
          self.size=frameno+1
      else:
        print "Note: Not reading line starting with ",fields[0]        
      line=f.readline()
      
      
  def readV1(self,f):
    header=f.readline()
    assert(header=="TITLE,MODELNO,FRAMENO,TIME,X,Y,Z,QX,QY,QZ,QW,SOURCE\n")
    line=f.readline()
    self.known_frames.clear()

    while line:

      fields=line.strip().split(",")
      if fields[0]=="TIMEPOINT":
        if fields[11]=="Labelled":
          frameno=int(fields[2])
          x=float(fields[4])
          y=float(fields[5])
          z=float(fields[6])
          qx=float(fields[7])
          qy=float(fields[8])
          qz=float(fields[9])
          qw=float(fields[10])
          self.known_frames[frameno]=Pose(x,y,z,qx,qy,qz,qw)
          if self.changingsize and frameno>=self.size:
            self.size=frameno+1
        elif fields[11]<>"Interpolated":
          print >> sys.stderr , "WARNING: Unknown field source ",fields[11],"--"
          print "WARNING: Unknown field source ",fields[11],"--"
      else:
        print "Note: Not reading line starting with ",fields[0]
      line=f.readline()  
    