from __future__ import print_function
from pickle import load as pload
from sys import argv
from os import system

if len(argv) == 2:
	MortrackPhysicsDataPath = 'D:/Users/Edward Kim/Documents'
	target_vid = argv[1]
elif len(argv) == 3:
	MortrackPhysicsDataPath = argv[2]
	target_vid = argv[1]
else:
	print('Intended usage:\npython run_label_vid.py target_vid [MortrackPhysicsDataPath]\n You used: \n' + str(argv) + '\nJust the name of the video should be supplied. The path will be inferred from the database and the third argument if it is given\n NOTE: This assumes Windows command prompt is the default system command handler')

with open('calibs.pickle','rb') as calib_file:
	calib_dicts = pload(calib_file)
	

model_file = MortrackPhysicsDataPath + calib_dicts[0][target_vid]['ModelFilename'][2:]
#print(model_file)
video_file = MortrackPhysicsDataPath + calib_dicts[1][target_vid][2:] + target_vid
intrinsics_file = MortrackPhysicsDataPath + calib_dicts[0][target_vid]['CameraCalibrationFilename'][2:]
extrinsics_file = MortrackPhysicsDataPath + calib_dicts[0][target_vid]['PoseCalibrationFilename'][2:]
labelled_data = ''


output = 'python2 label_vid.py "{0:s}" "{1:s}" "{2:s}" "{3:s}" "{4:s}"'.format(model_file, video_file, intrinsics_file, extrinsics_file, labelled_data)

system(output)