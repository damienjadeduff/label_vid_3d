#!/usr/bin/env python


'''     @package LabelVid3D
        pose_array_error from the package LabelVid3D
          
        Calculate vertex error in image space of two label files.

        --------------------------------------------------------------------
        
        Copyright (C) 2013  Damien Jade Duff (Delfina's Dad)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
            
'''        

import sys
import os
import calib
import pose_array
import time



try:
  from PyQt4 import QtCore, QtGui, QtOpenGL
  import ImageQt
except ImportError:
  print "PyQt must be installed to run this (provides OpenGL context; a bit circuitous but it was easy to implement)."
  sys.exit(1)
  
try:
  from OpenGL import GL
  from OpenGL import GLU
except ImportError:
  app = QtGui.QApplication(sys.argv)
  QtGui.QMessageBox.critical(None, "Pose array error",
          "PyOpenGL must be installed to run this.")
  sys.exit(1)
  
try:
  from pyassimp import pyassimp
except ImportError:
  app = QtGui.QApplication(sys.argv)
  QtGui.QMessageBox.critical(None, " Pose array error",
          "PyASSIMP must be installed to run this.")
  sys.exit(1)  


  #sys.exit(1)  
  
def imDist(pt1,pt2):
  return (pt1[0]-pt2[0])**2 + (pt1[1]-pt2[1])**2 
  
class Projector(QtOpenGL.QGLWidget):
  def __init__(self,calib_params,model):
    super(Projector, self).__init__()
    self.calib_params=calib_params
    self.setFixedSize(QtCore.QSize(self.calib_params.intrinsics_cam.w,self.calib_params.intrinsics_cam.h))
    self.model=model
    
  def initializeGL(self):
    pass
    #print "entering initializeGL()"
    ##self.qglClearColor(self.trolltechPurple.dark())
    #GL.glShadeModel(GL.GL_FLAT)
    #GL.glDisable(GL.GL_DEPTH_TEST)
    #GL.glEnable(GL.GL_CULL_FACE)
    #print "leaving initializeGL()"
    
  def resizeGL(self, width, height):
    
    #assert width==self.calib_params.intrinsics_cam.w
    #assert height==self.calib_params.intrinsics_cam.h
    
    #print "entering resizeGL()"
    #side = min(width, height)
    #if side < 0:
        #return

    #GL.glViewport(0, 0, width,height)
    
    ##GL.glMatrixMode(GL.GL_PROJECTION)
    ##GL.glLoadIdentity()

    ##GL.glMultMatrixf(self.calib_params.intrinsics_gl.instrmat);
    ##GL.glDepthRange(self.calib_params.intrinsics_cam.zNear,self.calib_params.intrinsics_cam.zFar)

    ##GL.glMatrixMode(GL.GL_MODELVIEW)
    #print "leaving resizeGL()"
    pass
      
  def projectPoint(self,pose,pt):
    #viewport = GL.glGetIntegerv( GL.GL_VIEWPORT )
    #print viewport
    #print dir(viewport)
    #print type(viewport)
    
    GL.glMatrixMode(GL.GL_PROJECTION)
    GL.glLoadIdentity()
    
    GL.glMultMatrixf(self.calib_params.intrinsics_gl.instrmat)
    
    GL.glMatrixMode(GL.GL_MODELVIEW)
    GL.glLoadIdentity()

    GL.glMultMatrixf(self.calib_params.extrinsics_gl.extrmat)

    GL.glMultMatrixf(pose.getGLMultMatrix())

                          #projection_matrix=GL.glGetIntegerv(GL.GL_MODELVIEW_MATRIX)
    #projection_matrix=GL.glGetDoublev(GL.GL_MODELVIEW_MATRIX)
                      #projection_matrix=GL.glGetFloatv(GL.GL_MODELVIEW_MATRIX)

    viewport = [0,0,self.calib_params.intrinsics_cam.w,self.calib_params.intrinsics_cam.h]
    #print "------------",pt[0],pt[1],pt[2],"  \n ",self.calib_params.intrinsics_gl.instrmat,"  \n  ",self.calib_params.extrinsics_gl.extrmat,"  \n ",viewport
    #x,y,z = GLU.gluProject(pt[0],pt[1],pt[2],self.calib_params.intrinsics_gl.instrmat,None,viewport) 
    #x,y,z = GLU.gluProject(pt[0],pt[1],pt[2],self.calib_params.intrinsics_gl.instrmat,projection_matrix,viewport) 
    #x,y,z = GLU.gluProject(pt[0],pt[1],pt[2],self.calib_params.intrinsics_gl.instrmat,projection_matrix.flatten('C'),viewport) 
    x,y,z = GLU.gluProject(pt[0],pt[1],pt[2],None,None,viewport) 
    #x,y,z = GLU.gluProject(pt[0],pt[1],pt[2],self.calib_params.intrinsics_gl.instrmat,self.calib_params.extrinsics_gl.extrmat,viewport) 
    return (x,y,z)
    #def paintGL(self):
    #pass
      #GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
      #GL.glLoadIdentity()
      
      
      #GL.glMultMatrixf(self.calib_params.extrinsics_gl.extrmat)
      
      
  def projectMesh(self,pose):
    mesh=self.model.meshes[0]
    vertices=mesh.vertices
    faces=mesh.faces    
    projected_vertices=[]
    for fno in range(0,len(faces)):
      face=faces[fno]
      for vind in face.indices:
    
        v=vertices[vind]
        projected_v=self.projectPoint(pose,v)
        projected_vertices.append(projected_v)
        #print "projected_v",projected_v
        
    return projected_vertices



def error_usage():
  print
  print "Usage:"
  print "       pose_array_error  MODEL_FILE  INSTRINSIC_CALIBRATION_PARAMS  EXTRINSIC_CALIBRATION_PARAMS  LABEL_FILE_1  LABEL_FILE_2"
  print
  sys.exit(1)
  
  '''generator'''
def array_img_errors(poses_1,poses_2,calib_params,model):
  
  size=max(poses_1.size,poses_2.size)
  poses_1.size=size
  poses_2.size=size

  projector = Projector(calib_params,model)
  projector.show()
  
  #print "starting"
  #stepdists=[]
  for timestep_cntr in range(0,size):

    projverts1=projector.projectMesh(poses_1[timestep_cntr])
    projverts2=projector.projectMesh(poses_2[timestep_cntr])
    
    assert(len(projverts1)==len(projverts2))
    
    stepdist=0
    
    for vertcntr in range(0,len(projverts1)):
      vertdist=imDist(projverts1[vertcntr],projverts2[vertcntr])
      stepdist=stepdist+vertdist
    #stepdists.append(stepdist)
    yield stepdist
    ##print timestep_cntr,",",stepdist,",",poses_1[timestep_cntr],",",poses_2[timestep_cntr]
  #plt.plot(stepdists)
  #plt.show()
  #return stepdists

  
def array_file_img_error(modelfile,calib_int,calib_ext,labels_filename_1,labels_filename_2,verbose=1):

  poses_1=pose_array.InterpolatingPoseArray()
  poses_2=pose_array.InterpolatingPoseArray()
  
  poses_1.fromFile(labels_filename_1)
  poses_2.fromFile(labels_filename_2)
      
  calib_params=calib.CalibFile(calib_int,calib_ext)
  
  model = pyassimp.load(modelfile)
  
  stepdists=array_img_errors(poses_1,poses_2,calib_params,model)
  timestep_cntr =0
  #for timestep_cntr in range(0,len(stepdists)):
  
  toret=[]
  for stepdist in stepdists:
    if verbose:
      print timestep_cntr,",",stepdist,",",poses_1[timestep_cntr],",",poses_2[timestep_cntr]
    timestep_cntr=timestep_cntr+1
    toret.append(stepdist)
  return toret
      
if __name__ == '__main__':
  
  app = QtGui.QApplication(sys.argv)



  try:
    print "Loading model file: "+sys.argv[1]
    modelfile=sys.argv[1]
  except IndexError:
    print "Pose array error: No model file supplied."
    error_usage()#sys.exit(1)

  calib_params=None
  
  try:
    calib_int=sys.argv[2]
  except IndexError:
    print "Pose array error No intrinsic calibration file supplied."
    error_usage()#sys.exit(1)        


  try:
    calib_ext=sys.argv[3]
  except IndexError:
    print "Pose array error: No extrinsic calibration file supplied."
    error_usage()#sys.exit(1)              
    


  
  try:
    labels_filename_1=sys.argv[4]
  except IndexError:
    print "no labels file (1) supplied"       
    error_usage()#sys.exit(1)
    
  try:
    labels_filename_2=sys.argv[5]
  except IndexError:
    print "no labels file (2) supplied"
    error_usage()#sys.exit(1)        
    

  stepdists=array_file_img_error(modelfile,calib_int,calib_ext,labels_filename_1,labels_filename_2)
  
  try:
    import matplotlib.pyplot as plt
  except ImportError:
    app = QtGui.QApplication(sys.argv)
    QtGui.QMessageBox.critical(None, " Pose array warning",
            "matplotlib must be installed to get pretty graphs.")
            
            
  plt.plot(stepdists)
  plt.show()
  