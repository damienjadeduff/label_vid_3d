#!/usr/bin/env python
# encoding: utf-8


'''     @package LabelVid3D
	calib from the package LabelVid3D - 
	  
	Calibration file loader. 
	Use at own risk, or fix at own expense ;)
	(executes arbitrary code)

        --------------------------------------------------------------------
        
        Copyright (C) 2013  Damien Jade Duff (Delfina's Dad)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
            
'''        




zFar_default = 4.0 # not supplied in parameters file
zNear_default = 0.1  # not supplied in parameters file



import copy
import string
import math
import sys

try:
  from OpenGL import GL
  from OpenGL import GLU
except ImportError:
  print "PyOpenGL must be installed to run this."         
  sys.exit(1)

try:
  import numpy
  import numpy.linalg
except ImportError:
  print "Numpy must be installed to run this."         
  sys.exit(1)


invertrot=numpy.zeros((4,4),'float')
invertrot[0,0]=1.0
invertrot[1,1]=-1.0
invertrot[2,2]=-1.0
invertrot[3,3]=1.0
  

def rotVectorToRotMatrix(rotvec):
  #vec needs to be 3x1 numpy
  vec=numpy.array(rotvec)
  #vec=numpy.zeros((3,1))
  vecnorm=numpy.linalg.norm(vec)
  #print vecnorm
  vecnormed=vec/vecnorm;
  #print vecnormed
  s=math.sin(vecnorm)
  c=math.cos(vecnorm)
  v=1-c
  x=vecnormed[0]*v
  y=vecnormed[1]*v
  z=vecnormed[2]*v
  rot=numpy.zeros((3,3),'float')
  
  rot[0,0]=vecnormed[0]*x+c
  rot[0,1]=vecnormed[0]*y-vecnormed[2]*s
  rot[0,2]=vecnormed[0]*z+vecnormed[1]*s
  
  rot[1,0]=vecnormed[1]*x+vecnormed[2]*s
  rot[1,1]=vecnormed[1]*y+c
  rot[1,2]=vecnormed[1]*z-vecnormed[0]*s
  
  rot[2,0]=vecnormed[2]*x-vecnormed[1]*s
  rot[2,1]=vecnormed[2]*y+vecnormed[0]*s
  rot[2,2]=vecnormed[2]*z+c
  
  return rot
    
class IntrinsicsCamera:
  def __init__(self,filename_internal):
    self.attr_dict={}
    self.text=""
    self.filename=filename_internal
    f = open(self.filename, 'r')
    full_file_contents=f.read()
    f.close()
    full_file_contents.index("# camera parameter file generated")
    try:
        cutoff=full_file_contents.index("# poses of calibration object w.r.t. camera for each image")
    except:
        cutoff=-1
    self.text=full_file_contents[0:cutoff]
    self.attr_dict={}
    exec(self.text,{},self.attr_dict)
    self.attr_dict["zFar"]=zFar_default
    self.attr_dict["zNear"]=zNear_default
    for key in self.attr_dict:
      val=self.attr_dict[key]
      setattr(self,key,val)    
  
class ExtrinsicsCamera:
  def __init__(self,filename_external):
    self.attr_dict={}
    self.text=""
    
    self.filename=filename_external
    f = open(self.filename, 'r')
    cont=True
    full_file_contents_1=""
    full_file_contents_2=""
    while cont:
      line=f.readline()
      #print line
      if not line:cont=False
      else:
        if len(line)>5 and line[0]<>"#":
          full_file_contents_1=full_file_contents_1+line
        else:
          full_file_contents_2=full_file_contents_2+line
    #f.readline()
    #full_file_contents=f.read()
    f.close()
    #full_file_contents.index("# camera parameter file generated")
    #cutoff=full_file_contents.index("# poses of calibration object w.r.t. camera for each image")
    full_file_contents_2.index("# Pose of camera w.r.t. calibration object:")
    #print "full file contents:" + full_file_contents_1
    self.text=full_file_contents_1.replace("pose = ","pose_translational=").replace("] [","]\npose_rotational=[").replace(" ",",")
    #print "ctext_external:" + self.text
    #self.ctext_internal=full_file_contents[0:cutoff]
    self.attr_dict={}
    exec(self.text,{},self.attr_dict)
    for key in self.attr_dict:
      val=self.attr_dict[key]
      setattr(self,key,val)          

      
class IntrinsicsGL:
  def __init__(self,cinstr):
    #comments and logic shortcutted from Thomas Mörwald; thanks Thom
    self.fx=2*cinstr.fx / cinstr.w #scale range from [0 ... 640] to [0 ... 2]
    self.fy=2*cinstr.fy / cinstr.h #scale range from [0 ... 480] to [0 ... 2]
    self.cx = 1.0-(2.0*cinstr.cx / cinstr.w) # move coordinates from left to middle of image: [0 ... 2] -> [-1 ... 1] (not negative z value at w-division)
    self.cy = (2.0*cinstr.cy / cinstr.h)-1.0# flip and move coordinates from top to middle of image: [0 ... 2] -> [-1 ... 1] (not negative z value at w-division)
    self.z1 = (cinstr.zFar+cinstr.zNear)/(cinstr.zNear-cinstr.zFar); #entries for clipping planes
    self.z2 = 2*cinstr.zFar*cinstr.zNear/(cinstr.zNear-cinstr.zFar);
    intrinsic=numpy.zeros((16),'float')
    intrinsic[0]=self.fx;intrinsic[4]=0;intrinsic[8]=self.cx;intrinsic[12]=0;
    intrinsic[1]=0;intrinsic[5]=self.fy;intrinsic[9]=self.cy;intrinsic[13]=0;
    intrinsic[2]=0;intrinsic[6]=0;intrinsic[10]=self.z1;intrinsic[14]=self.z2;  
    intrinsic[3]=0;intrinsic[7]=0;intrinsic[11]=-1;intrinsic[15]=0;
    
    self.instrmat=intrinsic
      
class ExtrinsicsGL:
  def __init__(self,cextr):
    # what we have here is pose of camera wrt calibration object. Need to invert that transform somehow.
    self.rot=rotVectorToRotMatrix(cextr.pose_rotational)
    self.tra=numpy.array(cextr.pose_translational)
    extrinsic_inv=numpy.zeros((4,4),'float')
    extrinsic_inv[0:3,0:3]=self.rot
    extrinsic_inv[0:3,3]=self.tra
    extrinsic_inv[3,3]=1.0
    extrinsic=numpy.linalg.inv(extrinsic_inv)
    #extrinsic_ready=extrinsic
    extrinsic_ready=numpy.dot(invertrot,extrinsic) #negative z axis into scene in opengl, but still right-handed
    
    self.extrmat=extrinsic_ready.T.reshape(16)# make it column major
    
    ###negVecRotted=-1*numpy.dot(self.rot.T,numpy.array(self.tra))
    ###extrinsic = numpy.zeros((4,4),'float')
    ###extrinsic[0:3,0:3]=self.rot 
    ####extrinsic[0:3,0:3]=numpy.identity(3,'float')########!!!
    ###extrinsic[0:3,3]=negVecRotted
    ###extrinsic[0:3,3]=-negVecRotted########!!!
    ###extrinsic[3,3]=1.0

    
    ####invertrot[1,1]=1.0########!!!
    ####invertrot[2,2]=1.0########!!!
    ###rottedextr=numpy.dot(invertrot,extrinsic)
    ####rottedextr=extrinsic
    ###self.extrmat=rottedextr.T.reshape(16) # make it column major
    #print self.rot
    #print self.tra
    #print extrinsic
    #print invertrot
    #print rottedextr
    #print self.extrmat
    
      
class CalibFile:
  
  
  def __init__(self,filename_internal,filename_external):
    self.intrinsics_cam=IntrinsicsCamera(filename_internal)
    self.extrinsics_cam=ExtrinsicsCamera(filename_external)
    self.intrinsics_gl=IntrinsicsGL(self.intrinsics_cam)	
    self.extrinsics_gl=ExtrinsicsGL(self.extrinsics_cam)
    #self.loadIntrinsic(filename_internal,self.intrinsics)
    #self.loadExtrinsic(filename_external,self.extrinsics)


  def print_raw_dict(self):
    print "INTERNAL: "
    for a in self.intrinsics_cam.attr_dict:
      print str(a)+" : "+str(self.intrinsics_cam.attr_dict[a]) +" "+str(type(self.intrinsics_cam.attr_dict[a]))
    print "EXTERNAL: "
    for a in self.extrinsics_cam.attr_dict:
      print str(a)+" : "+str(self.extrinsics_cam.attr_dict[a]) +" "+str(type(self.extrinsics_cam.attr_dict[a]))      
    
    
  #def getIntrinsicsMatrix(self):
    #return self.intrinsics_gl.projmat
    
  #def project_pointds
calib_file_internal =   "/media/TI30596700A/Users/docean/Desktop/MortrackPhysics/MortrackPhysicsData/RustamPushes20100625/RustamPushes20100624Intrinsic.cal"
calib_file_external =   "/media/TI30596700A/Users/docean/Desktop/MortrackPhysics/MortrackPhysicsData/RustamPushes20100625/RustamPushes20100624Pose.cal"
    
#calib_file = "/media/TI30596700A/Documents and Settings/docean/Desktop/MortrackPhysics/MortrackPhysicsProg/cam.cal"
#calib_file="./runtest.sh"


if __name__ == '__main__':
  calibi=CalibFile(calib_file_internal,calib_file_external)
  calibi.print_raw_dict()
  print "((((("
  print calibi.intrinsics_gl.instrmat
  
  
  #print dir(calib)
  #f = open(calib_file, 'r')
  #calib=f.read()
  #calib.index("# camera parameter file generated")
  #cutoff=calib.index("# poses of calibration object w.r.t. camera for each image")
  #calib=calib[0:cutoff]
  #locs=copy.copy(locals())
  #locs={}
  #exec(calib,{},locs)
  #print "locals = "
  
  #for a in locs:
    #print str(a)+" : "+str(locs[a]) +" "+str(type(locs[a]))
  
  #print fi
  #if fi==-1:
    #print "BAD INPUT FILE"
  #else:
    #print "FILE IS A CALIB FILE"
  
