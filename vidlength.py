#!/usr/bin/env python
import sys
import cv 
vidfilename = sys.argv[1]
cap=cv.CaptureFromFile(vidfilename)
if cap is None or not cap:
  sys.exit(1)
length=0
img=cv.QueryFrame(cap)
while img:
  length=length+1
  img=cv.QueryFrame(cap)
if length>0:
  print vidfilename,length
sys.exit(0)