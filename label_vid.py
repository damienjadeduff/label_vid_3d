#!/usr/bin/env python


'''     @package LabelVid3D
	label_vid from the package LabelVid3D - 
	  
	For labelling 3D videos for tracking (note that tracking results are not expected to be checked in the pose space, rather the projected image space)

        --------------------------------------------------------------------
        
        Copyright (C) 2013  Damien Jade Duff (Delfina's Dad)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
            
'''        


import pose_array
win_move_multiplier=1
default_rot_order="rxyz"
track_sphere_rad=0.12
# initial_pose=pose_array.Pose(0,0,0,0,0,0,1)
initial_pose=pose_array.Pose(-0.146551,-0.141577,0.0365009,0,0,0,1)

import sys
import math
import os
import calib
#import copy
import re
#import transformations


try:
  import numpy
  import numpy.linalg
except ImportError:
  print "Numpy must be installed to run this."         
  sys.exit(1)

try:
  from PyQt4 import QtCore, QtGui, QtOpenGL
except ImportError:
  print "PyQt must be installed to run this."
  sys.exit(1)
  
try:
  from OpenGL import GL
  from OpenGL import GLU
  from OpenGL import GLUT
  GLUT.glutInit()
except ImportError:
  app = QtGui.QApplication(sys.argv)
  QtGui.QMessageBox.critical(None, "Video labelling",
          "PyOpenGL must be installed to run this.")
  sys.exit(1)

try:
  import pyassimp
except ImportError:
  app = QtGui.QApplication(sys.argv)
  QtGui.QMessageBox.critical(None, " Video labelling",
          "PyASSIMP must be installed to run this.")
  sys.exit(1)  
  
try:
  import cv2
except ImportError:
  app = QtGui.QApplication(sys.argv)
  QtGui.QMessageBox.critical(None, " Video labelling",
          "python-opencv must be installed to run this.")
  sys.exit(1)  

try:
  from PIL import Image
  from PIL import ImageQt
except ImportError:
  app = QtGui.QApplication(sys.argv)
  QtGui.QMessageBox.critical(None, " Video labelling",
          "Python Imaging Library (PIL) must be installed to run this.")
  sys.exit(1)  

def debug_trace():
  '''Set a tracepoint in the Python debugger that works with Qt'''
  from PyQt4.QtCore import pyqtRemoveInputHook
  from pdb import set_trace
  pyqtRemoveInputHook()
  set_trace()    
    

class RandomAccessVid:
  def __init__(self,videofilename,buffersize=0):
    self.videofilename=videofilename
    self.buffersize=buffersize
    
    self.images=[]
    
    self.startframe=-1
    self.endframe=-1

    self.length=0   
    
    if buffersize>0:
      self.readIn(0,buffersize-1)

    
  def readIn(self,startframe,endframe):
    assert(endframe>startframe)
    #cap=cv.CaptureFromFile(self.videofilename)
    cap = cv2.VideoCapture(self.videofilename)
    cntr=-1
    for ind in range(0,len(self.images)):
      img=self.images[ind]
      self.images[ind]=None
      del(img)
    self.images=None
    self.images=[]
    # img=cv2.QueryFrame(cap)
    ret, img = cap.read()

    calclength=True
    if self.length>0: calclength=False
    self.length=0
    while ret and (calclength or cntr<=endframe+3):
      
      cntr+=1
      #print "Checkign frame "+str(cntr)
      if calclength:
        self.length+=1
      if cntr>=startframe and cntr<=endframe:
       #   print "Adding frame "+str(cntr)#
          # img2=cv.CreateImage((img.width,img.height),img.depth,img.nChannels)
          # cv2.Copy(img,img2)
          self.images.append(img)
      # ret,img=cv2.QueryFrame(cap)
      ret, img = cap.read()

      
    if cntr<startframe:
      self.startframe=-1
      self.endframe=-1
    elif cntr<endframe:
      self.startframe=startframe
      self.endframe=cntr
    else:
      self.startframe=startframe
      self.endframe=endframe
    #cv.ReleaseCapture(cap)
    del(cap)
     
    
  def getFrame(self,frameno): #can block a while
    if self.startframe==-1 or frameno<self.startframe or frameno>self.endframe:
      sf = frameno-self.buffersize/2
      ef= frameno+self.buffersize/2
      if ef-sf+1 > self.buffersize:
        ef-=1
      self.readIn(sf,ef)
    if frameno>=self.startframe and frameno<=self.endframe:
      #print "returning frame "+str(frameno)+" --- > "+str(frameno-self.startframe)
      return self.images[frameno-self.startframe]
    else:
      return None
        

class Controllers(QtGui.QDockWidget):
    def __init__(self,numFrames,parent):
        QtGui.QDockWidget.__init__(self)
        self.container=QtGui.QWidget(self)
        self.layout=QtGui.QBoxLayout (QtGui.QBoxLayout.TopToBottom,self.container)
        self.container.setLayout(self.layout)
        #self.frameslider=QtGui.QSlider(self.container)
        self.frameslider=QtGui.QSpinBox(self.container)
##        self.zoomslider=QtGui.QSlider(self.container)
        #self.frameslider.setTickInterval(numFrames)
        self.frameslider.setMaximum(numFrames-1)
        self.frameslider.setMinimum(0)
##        self.zoomslider.setMaximum(200)
##        self.zoomslider.setMinimum(-200)


        self.delbutton=QtGui.QPushButton("Frame delete labelling",self.container)
        
        self.copybutton=QtGui.QPushButton("Copy frame",self.container)
        self.pastebutton=QtGui.QPushButton("Paste frame",self.container)
        
        self.loadbutton=QtGui.QPushButton("Load Labelled Traj",self.container)
        self.savebutton=QtGui.QPushButton("Save Labelled Traj",self.container)

        
        self.statuslabel=QtGui.QLabel()
        
        self.sensitivityslider=QtGui.QSpinBox(self.container)  
        self.sensitivityslider.setMaximum(100)
        self.sensitivityslider.setMinimum(1)   
        self.sensitivityslider.setValue(10)
        
        self.ordertext=QtGui.QLineEdit(default_rot_order)
        
        self.layout.addWidget(self.statuslabel)
        self.layout.addWidget(QtGui.QLabel("Video Length: {0:d}".format(numFrames)))
        self.layout.addWidget(QtGui.QLabel("Set frame no:"))
        self.layout.addWidget(self.frameslider) 
        self.layout.addWidget(self.delbutton)
        self.layout.addWidget(self.copybutton)
        self.layout.addWidget(self.pastebutton)
        self.layout.addWidget(QtGui.QLabel("File controls:"))
        self.layout.addWidget(self.loadbutton)
        self.layout.addWidget(self.savebutton)           
        self.layout.addWidget(QtGui.QLabel("GUI sensitivity:"))
        self.layout.addWidget(self.sensitivityslider)
        self.layout.addWidget(QtGui.QLabel("Rotation order:"))
        self.layout.addWidget(self.ordertext)
                
        

        #self.xSlider = self.createSlider()
        #self.ySlider = self.createSlider()
        #self.zSlider = self.createSlider()
        

        ##
        #self.layout.addWidget(self.xSlider)
        #self.layout.addWidget(self.ySlider)
        #self.layout.addWidget(self.zSlider)
        #self.setLayout(mainLayout)
        
        
##        self.layout.addWidget(self.zoomslider)
        self.setWidget(self.container)
        self.setFloating(True)
        #self.frameslider.show()
        
    def createSlider(self):
        slider = QtGui.QSlider(QtCore.Qt.Vertical)

        slider.setRange(0, 1000)
        slider.setSingleStep(1)
        slider.setPageStep(100)
        slider.setTickInterval(100)
        slider.setTickPosition(QtGui.QSlider.TicksRight)

        return slider        
        
        
class Window(QtGui.QMainWindow):
    def __init__(self,modelfile,videofile,calib_params):
        super(Window, self).__init__()

        self.glWidget = GLWidget(modelfile,videofile,calib_params,self)

        self.dockframe=Controllers(self.glWidget.video.length,self)
        self.dockframe.frameslider.valueChanged.connect(self.glWidget.setFrame)
        self.dockframe.sensitivityslider.valueChanged.connect(self.glWidget.setSensitivity)
        self.dockframe.savebutton.pressed.connect(self.glWidget.saveTraj)
        self.dockframe.loadbutton.pressed.connect(self.glWidget.loadTraj)
        self.dockframe.delbutton.pressed.connect(self.glWidget.delFrame)
        self.dockframe.ordertext.textEdited.connect(self.glWidget.setOrder)
        self.dockframe.copybutton.pressed.connect(self.glWidget.clipboardCopyFrame)
        self.dockframe.pastebutton.pressed.connect(self.glWidget.clipboardPasteFrame)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea,self.dockframe)        


        #self.dockframe.xSlider.valueChanged.connect(self.glWidget.setXRotation)
        #self.glWidget.xRotationChanged.connect(self.dockframe.xSlider.setValue)
        #self.dockframe.ySlider.valueChanged.connect(self.glWidget.setYRotation)
        #self.glWidget.yRotationChanged.connect(self.dockframe.ySlider.setValue)
        #self.dockframe.zSlider.valueChanged.connect(self.glWidget.setZRotation)
        #self.glWidget.zRotationChanged.connect(self.dockframe.zSlider.setValue)

        
        self.setCentralWidget(self.glWidget)

        #self.dockframe.xSlider.setValue(0)
        #self.dockframe.ySlider.setValue(0)
        #self.dockframe.zSlider.setValue(0)
        
        
        self.glWidget.frame_changed.connect(self.dockframe.statuslabel.setText)
        #self.glWidget.labelled_frames_changed.connect(self.dockframe.frameslider.setToolTip)
        self.glWidget.labelled_frames_changed.connect(self.dockframe.setToolTip)
        self.glWidget.emitLabelledFrames()
        #self.glWidget.labelled_frames_changed.connect(self.dockframe.delbutton.setToolTip)

        self.setWindowTitle("Label 2D projection of 3D tracking")
        self.dockframe.frameslider.setValue(0)
        
                
        self.updater=QtCore.QTimer()
        #updater.setInterval(500)
        self.updater.timeout.connect(self.glWidget.updateGL)
        #updater.
        self.updater.start(200)

    def sizeHint(self):
      return QtCore.QSize(860,600)
      #return QtCore.QSize(740,480)
    def minimumSizeHint(self):
      return QtCore.QSize(800,600)      
    
      
class GLWidget(QtOpenGL.QGLWidget):
    xRotationChanged = QtCore.pyqtSignal(int)
    yRotationChanged = QtCore.pyqtSignal(int)
    zRotationChanged = QtCore.pyqtSignal(int)
    frame_changed=QtCore.pyqtSignal(str)
    labelled_frames_changed=QtCore.pyqtSignal(str)
    

    #def doUpdate(self):
      #print "timer"
      #self.updateGL()
    
    def __init__(self, modelfile, videofile,calib_params=None,parent=None):
        super(GLWidget, self).__init__(parent)

        self.guisensitivity=10
        self.rot_order=default_rot_order
        self.object = 0
        self.point_on_sphere=None
        self.objdisplaced=None
        self.rotvector=None
        self.labels_filename=None
        #self.xRot = 0
        #self.yRot = 0
        #self.zRot = 0
        
        #self.xTra = 0 
        #self.yTra = 0 
        #self.zTra = -0.5
        

        self.calib_params=calib_params
        
        self.lastPos = QtCore.QPoint()
        #self.moveType="none"

        self.trolltechGreen = QtGui.QColor.fromCmykF(0.40, 0.0, 1.0, 0.0)
        self.trolltechPurple = QtGui.QColor.fromCmykF(0.39, 0.39, 0.0, 0.0)
        
        self.model = pyassimp.load(modelfile)
        print "Model file loaded: "+modelfile
        #self.video = RandomAccessVid(videofile,100)#buffer 3 seconds
        #debug_trace()
        print "Loading video: "+videofile
        self.video = RandomAccessVid(videofile,2000)#buffer about half a minute
        if not self.video.length>0:
          print "\n\nVideo could not be loaded"
          assert(self.video.length>0)
        
        print "Video file loaded: "+videofile
        self.poses = pose_array.InterpolatingPoseArray(self.video.length)
        

        self.gl_textureid=-111
        self.currframe=-1
        #self.poses[0]=Pose(0,0,-0.5,0,0,-0,1)
        #self.poses[0]=Pose(0,0,0,0,0,0,1)
        #self.poses[0]=pose_array.Pose(0.00317963,0.235162,0.0525141,0.0519717,-0.690855,-0.00627259,0.721096)
        self.poses[0]=initial_pose
        self.clipboard=None
        #self.emitLabelledFrames()
        #self.setFrame(0)

        
    def defaultFileName(self):
      return self.video.videofilename+".labelledv2.txt"
      
        
    def saveTraj(self):
      framerate_s, ok = QtGui.QInputDialog.getText(self, 'Input Dialog', 'Enter your frame-rate:',text="30")

      if not ok or len(framerate_s)==0:
            QtGui.QMessageBox.warning(self,"Video labelling","Not saved - no framerate supplied.")
            return

      towrite=self.poses.toSaveString(1.0/int(framerate_s))
      print "will write "+towrite
      
      if self.labels_filename is not None:
        defaultsavename=os.path.realpath(self.labels_filename)
        #defaultsavename=os.path.realpath(os.path.split(self.labels_filename)[0])
      else:
        defaultsavename="./"+self.defaultFileName()
      
      
      print defaultsavename
      filename = QtGui.QFileDialog.getSaveFileName(self, 'Save Labelled Trajectory File',defaultsavename)
      #filename = QtGui.QFileDialog.getSaveFileName(self, 'Save Labelled Trajectory File',"./"+self.video.videofilename+".labelledv2.txt")
      if len(filename)==0:
          QtGui.QMessageBox.warning(self,"Video labelling","Not saved - no filename supplied.")
          return
      fname = open(filename, 'w')
      print "saving to "+filename
      
      
      fname.write(towrite)
      fname.close() 

    def loadTraj(self,filename=None):

      if not filename:
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Load Labelled Trajectory File',".")
      else:
        self.labels_filename=filename
        if not os.path.exists(filename):
          QtGui.QMessageBox.warning(self,"Video labelling","Not loaded - file provided on command line does not exist.")
          return
          
          #print filename
          #QtGui.QMessageBox.warning(self,"Video labelling","Not loaded - no filename supplied.")
          
          #filename = QtGui.QFileDialog.getOpenFileName(self, 'Load Labelled Trajectory File',filename)
        
        
      if len(filename)==0:
          QtGui.QMessageBox.warning(self,"Video labelling","Not loaded - no filename supplied.")
          return
          
      self.labels_filename=filename
      
      #fname = open(filename, 'r')
      print "Loading from ="+filename
      
      self.poses.fromFile(filename)
      self.emitLabelledFrames()
      #fname.write(towrite)
      #fname.close() 
    def setSensitivity(self,sens):
      self.guisensitivity=sens
      
    def setFrame(self,frameno):
#      if self.gl_textureid == -111:
#        self.updateGL()
      if self.gl_textureid <> -111 and self.currframe<>frameno:
        self.currframe=frameno
        
        #print "setting frame "+str(frameno)
        self.img=self.video.getFrame(frameno)
        GL.glBindTexture(GL.GL_TEXTURE_2D,self.gl_textureid) # Start using that new texture
        GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP) # How textures repeat 
        GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP)
        GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
        GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)#turn off mipmapping                
        GL.glPixelStorei(GL.GL_UNPACK_ALIGNMENT,1)
       # imsize=
        #pil_img = Image.fromstring("L", (self.img.width,self.img.height), self.img.tostring())#IPL-->PIL
        # pil_img = Image.frombytes("RGB", (self.img.width,self.img.height), self.img.tostring())#IPL-->PIL

        pil_img = Image.frombytes("RGB", (self.img.shape[1],self.img.shape[0]), self.img.tostring())#IPL-->PIL

        b,g,r=pil_img.split()
        pil_img=Image.merge("RGB",(r,g,b))
        
        #Now load the texture into OpenGL memory. It is an RGBA list of floats.
      #  print(dir(self.img))
      #  print self.img

        #imgstring=Image.tostring(pil_img,"RGBX",1)
        #imgstring=pil_img.tostring("RGBX",1)
        imgstring=pil_img.convert("RGBX").tobytes()
        #imgstring=self.img.tostring()
        
      #  print (type(imgstring))
      #  print(len(imgstring))
       # print imgstring
        GL.glTexImage2D(GL.GL_TEXTURE_2D, 0,  GL.GL_RGBA,self.img.shape[1], self.img.shape[0], 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, imgstring)      
        #GL.glTexImage2D(GL.GL_TEXTURE_2D, 0,  GL.GL_RGBA,self.img.width, self.img.height, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE_2_3_3_REV, imgstring)      
        
        GL.glBindTexture(GL.GL_TEXTURE_2D,-1)
        #print "calling updateGL in setFrame"
        self.updateGL()
        #print "called updateGL in setFrame"
        #self.statuslabel.setText(self.poses.frameStatus(self.currframe))
        self.frame_changed.emit(QtCore.SIGNAL("[Frame "+str(self.currframe)+"]:"+self.poses.frameStatus(self.currframe)))

    def delFrame(self):
      self.poses[self.currframe]=None
      self.updateGL()
      self.emitLabelledFrames()
      
    def clipboardCopyFrame(self):
      self.clipboard=self.poses[self.currframe]
      
    def clipboardPasteFrame(self):
      self.poses[self.currframe]=self.clipboard
      self.updateGL()
      self.emitLabelledFrames()
      
    def minimumSizeHint(self):
        return QtCore.QSize(50, 50)

    def sizeHint(self):
        return QtCore.QSize(800, 600)
        #return QtCore.QSize(640, 480)


    #def setXRotation(self, angle):
        ##ss="xRotation "+str(self.poses[self.currframe].qx)+"["+str(self.poses[self.currframe].qw)+"]"
        #if self.currframe>=0:
          #e=self.poses[self.currframe].rotAsAxis()
          #e[0]=angle
          #self.poses[self.currframe]=self.poses[self.currframe].rotFromAxis(e)
          
          ##self.poses[self.currframe].normalizeQuat()
         ## print ss+" --> "+str(self.poses[self.currframe].qx)+"["+str(self.poses[self.currframe].qw)+"]"
    #def setYRotation(self, angle):
        #if self.currframe>=0:
          #e=self.poses[self.currframe].rotAsAxis()
          #e[1]=angle
          #self.poses[self.currframe]=self.poses[self.currframe].rotFromAxis(e)
    #def setZRotation(self, angle):
        #if self.currframe>=0:
          #e=self.poses[self.currframe].rotAsAxis()
          #e[2]=angle
          #self.poses[self.currframe]=self.poses[self.currframe].rotFromAxis(e)  
    #def getXRotation(self):
        #if self.currframe>=0:
          #e=self.poses[self.currframe].rotAsAxis()
          #return e[0]
        #else: return 0
    #def getYRotation(self):
        #if self.currframe>=0:
          #e=self.poses[self.currframe].rotAsAxis()
          #return e[1]
        #else: return 0
    #def getZRotation(self):
        #if self.currframe>=0:
          #e=self.poses[self.currframe].rotAsAxis()
          #return e[2]
          
        #else: return 0               
    def compose_axis_rot(self,rot):
      #debug_trace()
      if self.currframe>=0:
          newpose=self.poses[self.currframe].mult_axis(rot)
          self.poses[self.currframe]=newpose
    
    def setXRotation(self, angle):
        #ss="xRotation "+str(self.poses[self.currframe].qx)+"["+str(self.poses[self.currframe].qw)+"]"
        if self.currframe>=0:
          e=self.poses[self.currframe].rotAsEuler(self.rot_order)
          #print "X:",e.phi,angle
          e.phi=angle
          self.poses[self.currframe]=self.poses[self.currframe].rotFromEuler(e,self.rot_order)
          
          #self.poses[self.currframe].normalizeQuat()
         # print ss+" --> "+str(self.poses[self.currframe].qx)+"["+str(self.poses[self.currframe].qw)+"]"
    def setYRotation(self, angle):
        if self.currframe>=0:
          e=self.poses[self.currframe].rotAsEuler(self.rot_order)
          #print "Y:",e.theta,angle
          e.theta=angle
          self.poses[self.currframe]=self.poses[self.currframe].rotFromEuler(e,self.rot_order)
    def setZRotation(self, angle):
        if self.currframe>=0:
          e=self.poses[self.currframe].rotAsEuler(self.rot_order)
          #print "Z:",e.psi,angle
          e.psi=angle
          self.poses[self.currframe]=self.poses[self.currframe].rotFromEuler(e,self.rot_order)  
    def getXRotation(self):
        if self.currframe>=0:
          e=self.poses[self.currframe].rotAsEuler(self.rot_order)
          #if e.phi<0:
            #e.phi=e.phi+math.pi*2 # otherwise, when it is -1.5 / 1.5 it will get added to to get 1.5, then subtracted from to get -1.5 in a ponitless oscillation
          return e.phi
        else: return 0
    def getYRotation(self):
        if self.currframe>=0:
          e=self.poses[self.currframe].rotAsEuler(self.rot_order)
          #if e.theta<0:
            #e.theta=e.theta+math.pi*2 # otherwise, when it is -1.5 / 1.5 it will get added to to get 1.5, then subtracted from to get -1.5 in a ponitless oscillation
          return e.theta
        else: return 0
    def getZRotation(self):
        if self.currframe>=0:
          e=self.poses[self.currframe].rotAsEuler(self.rot_order)
          #if e.psi<0:
            #e.psi=e.psi+math.pi*2 # otherwise, when it is -1.5 / 1.5 it will get added to to get 1.5, then subtracted from to get -1.5 in a ponitless oscillation
          return e.psi
          
        else: return 0        
        #angle = self.normalizeAngle(angle)
        #if angle != self.xRot:
            #self.xRot = angle
            #self.xRotationChanged.emit(angle)           

    #def setYRotation(self, angle):
        #angle = self.normalizeAngle(angle)
        #if angle != self.yRot:
            #self.yRot = angle
            #self.yRotationChanged.emit(angle)

    #def setZRotation(self, angle):
        #angle = self.normalizeAngle(angle)
        #if angle != self.zRot:
            #self.zRot = angle
            #self.zRotationChanged.emit(angle)
    
    def moveWin(self,winx,winy,winz):
      #debug_trace()
      
      #print "before ",self.poses[self.currframe]
      transWorld=self.winMoveToWorldMoveV(winx,winy,winz)
      print "change",transWorld
      pose=self.poses[self.currframe]
      pose=pose.changeX(transWorld[0]+pose.x)
      pose=pose.changeY(transWorld[1]+pose.y)
      pose=pose.changeZ(transWorld[2]+pose.z)
      self.poses[self.currframe]=pose
      #print "after ",self.poses[self.currframe]
    
    def moveXTrans(self,transWin):
   #   print "changing xTra from "+str(self.xTra)
      transWorld=self.winMoveToWorldMove(transWin)
      if self.currframe>=0:
        self.poses[self.currframe]=self.poses[self.currframe].changeX(self.poses[self.currframe].x+transWorld)
    #  print "to "+str(self.xTra)+" using the translated dist of "+str(transWorld)+" (transWin="+str(transWin)+")"

    def moveYTrans(self,transWin):
   #   print "changing xTra from "+str(self.xTra)
      transWorld=self.winMoveToWorldMove(-transWin)
      if self.currframe>=0:
        self.poses[self.currframe]=self.poses[self.currframe].changeY(self.poses[self.currframe].y+transWorld)
    #  print "to "+str(self.xTra)+" using the translated dist of "+str(transWorld)+" (transWin="+str(transWin)+")"

    def moveZTrans(self,transWin):
   #   print "changing xTra from "+str(self.xTra)
      transWorld=self.winMoveToWorldMove(transWin)
      if self.currframe>=0:
        self.poses[self.currframe]=self.poses[self.currframe].changeZ(self.poses[self.currframe].z+transWorld)
    #  print "to "+str(self.xTra)+" using the translated dist of "+str(transWorld)+" (transWin="+str(transWin)+")"
    
    
    def xTra(self):
      if self.currframe>=0:
        return self.poses[self.currframe].x
      

    def yTra(self):
      if self.currframe>=0:
        return self.poses[self.currframe].y

    def zTra(self):
     # print "self.currframe="+str(self.currframe)
      if self.currframe>=0:
        res=self.poses[self.currframe].z
      #  print "res="+str(res)
        return  res
        
    #def moveYTrans(self,transWin):
      #self.yTra=self.yTra+self.winDistToWorldAmt(transWin)
      
    #def moveZTrans(self,transWin):
      #self.zTra=self.zTra+self.winDistToWorldAmt(transWin)      
      #print "new zTra="+str(self.zTra)

    def initializeGL(self):
      print "entering initializeGL()"
      self.qglClearColor(self.trolltechPurple.dark())
      #self.object = self.makeCallListObject()
      GL.glShadeModel(GL.GL_FLAT)
      GL.glDisable(GL.GL_DEPTH_TEST)
      GL.glEnable(GL.GL_CULL_FACE)
      self.gl_textureid=GL.glGenTextures(1) 
      print "leaving initializeGL()"
        

    def setOrder(self,order):
      self.rot_order=str(order)
      
      
    def drawBackground(self):
      depth=0.99 #depth buffer goes from 0.0 to 1.0
      viewport=GL.glGetIntegerv(GL.GL_VIEWPORT)
      modelview=GL.glGetDoublev(GL.GL_MODELVIEW_MATRIX)
      projection=GL.glGetDoublev(GL.GL_PROJECTION_MATRIX)      
      a=(x1,y1,z1)=GLU.gluUnProject(viewport[0],viewport[1],depth,modelview,projection,viewport)
      #import pdb;pdb.set_trace()      
      #print "drawBackground -texture_id="+str(self.gl_textureid)
      GL.glColor(1.0,1.0,1.0)
      b=(x2,y2,z2)=GLU.gluUnProject(viewport[0]+viewport[2],viewport[1],depth,modelview,projection,viewport)
      c=(x3,y3,z3)=GLU.gluUnProject(viewport[0]+viewport[2],viewport[1]+viewport[3],depth,modelview,projection,viewport)
      d=(x4,y4,z4)=GLU.gluUnProject(viewport[0],viewport[1]+viewport[3],depth,modelview,projection,viewport)
    #  print "a="+str(a)
    #  print "b="+str(b)
    #  print "c="+str(c)
    #  print "d="+str(d)
      GL.glEnable(GL.GL_TEXTURE_2D)
      GL.glBindTexture(GL.GL_TEXTURE_2D,self.gl_textureid)
      #x1=x1*20
      #x2=x2*20
      #y1=y1*20
      #y2=y2*20
      GL.glBegin(GL.GL_QUADS)
      GL.glTexCoord2f(0.0,1.0)
      GL.glVertex3d(x1,y1,z1)
      GL.glTexCoord2f(1.0,1.0)
      GL.glVertex3d(x2,y2,z2)
      GL.glTexCoord2f(1.0,0.0)
      GL.glVertex3d(x3,y3,z3)      
      GL.glTexCoord2f(0.0,0.0)
      GL.glVertex3d(x4,y4,z4)            
      GL.glEnd()
      GL.glBindTexture(GL.GL_TEXTURE_2D,-1)
      GL.glDisable(GL.GL_TEXTURE_2D)
      
    #def paintEvent(self,paintEvent):
    def paintGL(self):
        #print "paintEvent"
        #print "entering paintGL()"
        if self.currframe<0:self.setFrame(0);
        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
        GL.glLoadIdentity()

        #p=QtGui.QPainter();
        #p.begin(self)
        ##cols,rows=cv.GetSize(self.img)
        ##pil_img = Image.fromstring("L", (cols,rows), self.img.tostring())#IPL-->PIL
        ##qt_img=ImageQt.ImageQt(pil_img)
        ##img_qt=ImageQt.QImage(qt_img)
        ##p.drawPixmap(p.viewport(),QtGui.QPixmap.fromImage(img_qt))
        #p.drawRect(QtCore.QRect(50,50,100,100))
        #p.end()
    #def paintGL():
        
        #GL.glTranslated(0.0, 0.0, -10.0)
        #print "tra"
        #print self.xTra
        #print self.yTra
        #print "---"        
        #GL.glTranslated(self.xTra,self.yTra,self.zTra)
        #GL.glRotated(self.xRot / 16.0, 1.0, 0.0, 0.0)
        #GL.glRotated(self.yRot / 16.0, 0.0, 1.0, 0.0)
        #GL.glRotated(self.zRot / 16.0, 0.0, 0.0, 1.0)
        #print "checking current frame"
        if self.currframe>=0:
          self.drawBackground()
          
          if self.calib_params:
            GL.glMultMatrixf(self.calib_params.extrinsics_gl.extrmat)
          
          
          #drawtrackballdebug=True
          #if drawtrackballdebug:

            ##if self.objdisplaced is not None:
              ###print "doing objdisplaced"
              ##GL.glColor(0.0,0.0,1.0)
              ##GL.glPushMatrix()
              ##GL.glTranslated(self.objdisplaced[0],self.objdisplaced[1],self.objdisplaced[2])
              ##GLUT.glutWireSphere(0.01,3,3)
              ##GL.glPopMatrix()
                            
          if self.point_on_sphere is not None:
            # GL.glColor4f(1.0,1.0,0.3,0.5)
            # GL.glPushMatrix()
            # GL.glTranslated(self.poses[self.currframe].x,self.poses[self.currframe].y,self.poses[self.currframe].z)
            # GLUT.glutWireSphere(track_sphere_rad,9,9)
            # GL.glPopMatrix()              
            
            # #print "doing sphere"
            # GL.glColor4f(1.0,0.0,0.0,0.2)
            # GL.glPushMatrix()
            # GL.glTranslated(self.point_on_sphere[0],self.point_on_sphere[1],self.point_on_sphere[2])
            # GLUT.glutWireSphere(0.01,3,3)
            # GL.glPopMatrix()
            
            if self.rotvector is not None:
              #print "doing rotvector"
              GL.glColor(0.0,1.0,0.0)
              GL.glPushMatrix()
              GL.glTranslated(self.poses[self.currframe].x,self.poses[self.currframe].y,self.poses[self.currframe].z)
              GL.glBegin(GL.GL_LINES)
              GL.glVertex3f(0,0,0)
              GL.glVertex3f(self.rotvector[0],self.rotvector[1],self.rotvector[2])
              GL.glEnd()
              
              GL.glPopMatrix()     
              
            self.point_on_sphere=None
            #self.updateGL()
        
          #print "Drawing frame "+str(self.currframe)
          GL.glPushMatrix()
          
          
          
          self.poses[self.currframe].multGL()
          #GL.glCallList(self.object)
          
          self.drawObject()
          

          GL.glPopMatrix()
        #print "leaving paintGL()"

    #def paintGL(self):
        #GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
        #GL.glLoadIdentity()
        ##GL.glTranslated(0.0, 0.0, -10.0)
        ##print "tra"
        ##print self.xTra
        ##print self.yTra
        ##print "---"        
        #GL.glTranslated(self.xTra,self.yTra,self.zTra)
        #GL.glRotated(self.xRot / 16.0, 1.0, 0.0, 0.0)
        #GL.glRotated(self.yRot / 16.0, 0.0, 1.0, 0.0)
        #GL.glRotated(self.zRot / 16.0, 0.0, 0.0, 1.0)
        
        #GL.glCallList(self.object)        

    def resizeGL(self, width, height):
        print "entering resizeGL()"
        side = min(width, height)
        if side < 0:
            return

        #GL.glViewport((width - side) / 2, (height - side) / 2, side, side)
        GL.glViewport(0, 0, width,height)
        
        #if self.calib_params:
          

        GL.glMatrixMode(GL.GL_PROJECTION)
        GL.glLoadIdentity()
        #GL.glOrtho(-0.5, +0.5, +0.5, -0.5, 4.0, 15.0)
        #GL.glOrtho(-1.0, +1.0, +1.0, -1.0, 0.02, 3.0)
        if self.calib_params: 
          #GL.glLoadMatrixf(self.calib_params.intrinsics_gl.intrinsic);
          GL.glMultMatrixf(self.calib_params.intrinsics_gl.instrmat);
          GL.glDepthRange(self.calib_params.intrinsics_cam.zNear,self.calib_params.intrinsics_cam.zFar)
        else:
          GLU.gluPerspective(45.0,1.0,0.02,3.0)
        GL.glMatrixMode(GL.GL_MODELVIEW)
        print "leaving resizeGL()"
          
    def mousePressEvent(self, event):
        #if event.button() == Qt.LeftButton:
          #self.moveType="XtYt"
        #elif event.button() == Qt.MidButton:
          #self.moveType="ZtZr"
        #elif event.button() == Qt.RightButton:
          #self.moveType="XrYr"
        #else:
          #self.moveType="oops"
        self.lastPos = event.pos()

    def mouseMoveEvent(self, event):
        self.point_on_sphere=None
        self.objdisplaced=None
        self.rotvector=None
        #mods=event.modifiers()
        #shiftclick = mods==QtCore.Qt.ShiftModifier
        #if shiftclick:
          #print "SHIFT"
        #print event.modifiers
        #import pdb;pdb.set_trace();QtCore.pyqtRemoveInputHook()
        dx = event.x() - self.lastPos.x()
        dy = event.y() - self.lastPos.y()
        rotfactor=0.001
        #print "dx="+str(dx)
        #print "dy="+str(dy)
        
        #print "before-----    "+str(self.poses[self.currframe].rotAsEuler(self.rot_order))
        #print "         before-----    "+str(self.poses[self.currframe])
        #print "order",self.rot_order
        
        leftmouse=event.buttons() & QtCore.Qt.LeftButton
        rightmouse=event.buttons() & QtCore.Qt.RightButton
        midmouse=event.buttons() & QtCore.Qt.MidButton
        
        shiftkey=event.modifiers() & QtCore.Qt.ShiftModifier
        altkey=event.modifiers() & QtCore.Qt.AltModifier
        ctrlkey=event.modifiers() & QtCore.Qt.ControlModifier
        
        
        somechange=False
        if leftmouse and not shiftkey and not ctrlkey:
          #print "shift"
          #self.moveWin(dx*self.guisensitivity,dy*self.guisensitivity)
          #if abs(dx)>abs(dy):
            #self.moveWin(dx*self.guisensitivity/10.0,0,0)
          #else:
            #self.moveWin(0,dy*self.guisensitivity/10.0,0)
          self.moveWin(dx*self.guisensitivity/10.0,dy*self.guisensitivity/10.,0)
          #self.moveWin(dx*self.guisensitivity,dy*self.guisensitivity)
          somechange=True
        elif rightmouse and not shiftkey and not ctrlkey:
          if abs(dx)<abs(dy):
            self.moveWin(0,0,dy*self.guisensitivity/10.0)
            
        elif leftmouse and not shiftkey and ctrlkey:
            #self.xTra=self.xTra + 0.01 * dx
            if abs(dx)>abs(dy):
              self.moveXTrans(dx*self.guisensitivity/10.0)
            else:
              self.moveYTrans(dy*self.guisensitivity/10.0)
            somechange=True
            #self.yTra=self.yTra + 0.01 * dy        
        elif leftmouse and shiftkey and not ctrlkey:
            #if abs(dx)>abs(dy):
              #self.setXRotation(self.getXRotation() - rotfactor * dx *self.guisensitivity)
            #else:
              #self.setYRotation(self.getYRotation()- rotfactor * dy*self.guisensitivity)
            rot=self.rotation_for_point_pair(self.lastPos.x(),self.lastPos.y(),event.x(),event.y())
            #print "()()()() ",  rot*self.guisensitivity/10.0,  rot
            #print self.poses[self.currframe] 
            #print "    ",rot*self.guisensitivity/10.0
            self.compose_axis_rot(rot*self.guisensitivity/10.0)
            #print "              ",self.poses[self.currframe]
            somechange=True
        
        #elif event.buttons() & QtCore.Qt.MidButton:  
        
        #elif rightmouse and shiftkey and not ctrlkey:  
            #if abs(dx)>abs(dy):
            #self.setZRotation(self.getZRotation() + rotfactor * dy*self.guisensitivity)
            #else:
              #self.moveZTrans(dy*self.guisensitivity)
            #somechange=True
            #self.zTra=self.zTra + 0.01 * dx
            
        elif leftmouse and shiftkey and ctrlkey:
            if abs(dx)>abs(dy):
              self.setXRotation(self.getXRotation() - rotfactor * dx *self.guisensitivity)
            else:
              self.setYRotation(self.getYRotation()- rotfactor * dy*self.guisensitivity)
            somechange=True
        
        #elif event.buttons() & QtCore.Qt.MidButton:  
        
        elif rightmouse and shiftkey and ctrlkey:  
            #if abs(dx)>abs(dy):
            self.setZRotation(self.getZRotation() + rotfactor * dy*self.guisensitivity)
            #else:
              #self.moveZTrans(dy*self.guisensitivity)
            somechange=True
            #self.zTra=self.zTra + 0.01 * dx

            
        #print "after-----    "+str(self.poses[self.currframe].rotAsEuler(self.rot_order))   
        #print "         after-----    "+str(self.poses[self.currframe])
        self.lastPos = event.pos()
        self.updateGL()
        if somechange:
          self.emitLabelledFrames()
        

      
      
    def emitLabelledFrames(self):
      labelled_frames=self.poses.labelledFrames()
      emitstr="Labelled frames: "+str(labelled_frames)
      emitstrQ=QtCore.QString(emitstr)
      self.labelled_frames_changed.emit(QtCore.SIGNAL(emitstrQ))
      #print "emitted ",emitstr,emitstrQ
    
    def makeCallListObject(self):
        genList = GL.glGenLists(1)
        GL.glNewList(genList, GL.GL_COMPILE)
        self.drawObject()
        GL.glEndList()
        return genList        
        
    def drawObject(self):
        
        #print "entering drawObject()"

        assert(len(self.model.meshes)==1)
        
        #print "MESHES:"
        #for index, mesh in enumerate(self.model.meshes):
            #print "  MESH", index+1
            ##print "    material:", mesh.mMaterialIndex+1
            #print "    vertices:", len(mesh.vertices)
            #print "    first:", mesh.vertices[:3]
            ##print "    colors:", len(mesh.colors)
            ##tc = mesh.texcoords
            ##print "    uv-component-count:", len(mesh.mNumUVComponents)
            #print "    faces:", len(mesh.faces), "first:", [f.indices for f in mesh.faces[:3]]
            #print        
            
      
        mesh=self.model.meshes[0]
        
        vertices=mesh.vertices
        faces=mesh.faces
        #GL.glBegin(GL.GL_TRIANGLES)
        GL.glLineWidth(3.0);
        #print "Model file has "+str(len(faces))+" faces and "+str(len(vertices))+" vertices (setting up call list now)."
        #fno=-1
        for fno in range(0,len(faces)):
        #for face in faces:
          face=faces[fno]
          
          #fno=fno+1
          #print "drawing face "+str(fno)        
          #if fno == 0:
          #  GL.glColor(1.0,0.0,0.0)
          #else:
          GL.glColor(0.0,0.0,1.0)
          GL.glBegin(GL.GL_LINE_STRIP)
          avgx=0
          avgy=0
          avgz=0
          for vind in face:#.indices:
              #print "vind="+str(vind)
              v=vertices[vind]
              
              GL.glVertex3d(v[0],v[1],v[2])
              avgx+=v[0]
              avgy+=v[1]
              avgz+=v[2]

          avgx=avgx/len(face)#.indices)
          avgy=avgy/len(face)#.indices)
          avgz=avgz/len(face)#.indices)
          
          #v1=vertices[face.indices[0]]
          #v2=vertices[face.indices[1]]
          #v3=vertices[face.indices[2]]
          #v=v1
          #GL.glVertex3d(v[0],v[1],v[2])
          #v=v2
          #GL.glVertex3d(v[0],v[1],v[2])
          #v=v3
          #GL.glVertex3d(v[0],v[1],v[2])
          #v=v1
          #GL.glVertex3d(v[0],v[1],v[2])
          GL.glEnd()
          if fno ==0:
            GL.glColor(1.0,0.0,0.0)
            for vind in face:#.indices:
                #print "vind="+str(vind)
                v=vertices[vind]
                #rendertext="{0:.1e},{1:.1e},{2:.1e}".format(v[0],v[1],v[2])
                #rendertext="{0:.1},{1:.1},{2:.1}".format(100*v[0],100*v[1],100*v[2])
                #self.renderText(v[0],v[1],v[2],rendertext)
                #self.renderText(v[0],v[1],v[2],str(v[0])+","+str(v[1])+","+str(v[2]))              
          GL.glColor(0.5,1.0,1.0)
          self.renderText(avgx,avgy,avgz,str(fno)) # doesn't work in glcalllist
        ###vind=0
        ###for v2 in vertices:
          
          ###self.renderText(v2[0],v2[1],v2[2],str(vind)) # doesn't work in glcalllist
          ###vind=vind+1
        

        
        
    def sizeHint(self):
      #return QtCore.QSize(640,480)
      return QtCore.QSize(800,600)
    def minimumSizeHint(self):
      #return QtCore.QSize(400,400)    
      return QtCore.QSize(400,400)    
    
    
    def rotation_for_point_pair(self,u1,v1,u2,v2):
      #print self.unproject_to_obj_sphere(u1,v1,track_sphere_rad)
      #print self.unproject_to_obj_sphere(u2,v2,track_sphere_rad)
      #print " ------ ",self.unproject_to_obj(u1,v1)
      #print " ------ ",self.unproject_to_obj(u2,v2)
      #debug_trace()
      world1=numpy.array(self.unproject_to_obj_sphere(u1,v1,track_sphere_rad))
      world2=numpy.array(self.unproject_to_obj_sphere(u2,v2,track_sphere_rad))
      #world1=numpy.array(self.unproject_to_obj(u1,v1))
      #world2=numpy.array(self.unproject_to_obj(u2,v2))
      objcenter=numpy.array([self.poses[self.currframe].x,self.poses[self.currframe].y,self.poses[self.currframe].z])
      #diff=np.array(a-b for (a,b) in zip(world2,world1))
      
      
      self.rotvector=(3.141577/track_sphere_rad) * numpy.cross(world2-objcenter,world1-objcenter)
      #self.rotvector=numpy.cross(world2-objcenter,world1-objcenter)*(1.0/track_sphere_rad)
      
      #print " ----------",self.rotvector
      
      #print "!!!!!!!!!!!",self.poses[self.currframe],self.poses[self.currframe].invert(),self.poses[self.currframe].only_rot().invert().transformPoint(self.rotvector)
      #print "???????????",self.poses[self.currframe].only_rot().transformPoint(self.rotvector)
      
      rotvector_objframe=numpy.array(self.poses[self.currframe].only_rot().transformPoint(self.rotvector))
      #rotvector_objframe=numpy.array(self.poses[self.currframe].only_rot().invert().transformPoint(self.rotvector))
      
      #print "^^^^^^^^^^^",rotvector_objframe
      return rotvector_objframe
    
    def unproject(self,u,v,depth):
    
      viewport=GL.glGetIntegerv(GL.GL_VIEWPORT)
      modelview=GL.glGetDoublev(GL.GL_MODELVIEW_MATRIX)
      projection=numpy.copy(GL.glGetDoublev(GL.GL_PROJECTION_MATRIX))

      new_world_coords=(pickptx,pickpty,pickptz)=GLU.gluUnProject(u,viewport[3]-v,depth,modelview,projection,viewport)
      return new_world_coords
      
      
    def unproject_to_obj(self,u,v):
      #debug_trace()
      depth = GL.glReadPixels(u, v, 1, 1, GL.GL_DEPTH_COMPONENT, GL.GL_FLOAT)
      #print depth
      return self.unproject(u,v,depth)
      
      
    def unproject_to_obj_sphere(self,u,v,spheresize):
      
      viewport=GL.glGetIntegerv(GL.GL_VIEWPORT)
      modelview=GL.glGetDoublev(GL.GL_MODELVIEW_MATRIX)
      projection=numpy.copy(GL.glGetDoublev(GL.GL_PROJECTION_MATRIX))
      objcoords=numpy.array([self.poses[self.currframe].x,self.poses[self.currframe].y,self.poses[self.currframe].z])

      objwincoords=(objWinX,objWinY,objWinZ)=GLU.gluProject(objcoords[0],objcoords[1],objcoords[2],modelview,projection,viewport)
      self.objdisplaced=numpy.array(GLU.gluUnProject(u,viewport[3]-v,objWinZ,modelview,projection,viewport))
      clickpt=numpy.array(GLU.gluUnProject(u,viewport[3]-v,0.0,modelview,projection,viewport))
      fromclicktodisplaced=self.objdisplaced-clickpt
      
      obj_to_displaced=self.objdisplaced-objcoords
      
      dist_from_sphere_origin=numpy.sqrt(obj_to_displaced.dot(obj_to_displaced))
      
      if dist_from_sphere_origin>spheresize:
        dist_from_sphere_origin=spheresize
      
      reduction_dist=math.sqrt(spheresize**2-dist_from_sphere_origin**2)
      
      totaldist=numpy.sqrt(fromclicktodisplaced.dot(fromclicktodisplaced))
      
      if reduction_dist>totaldist:
        reduction_dist=totaldist-0.000001
      
      ratio=(totaldist-reduction_dist)/totaldist
      
      self.point_on_sphere=ratio*fromclicktodisplaced+clickpt
      
      spherecentertopoint=self.point_on_sphere-objcoords
      
      #print "dist to point on sphere: ",numpy.sqrt(spherecentertopoint.dot(spherecentertopoint)), "displaced pt: ",dist_from_sphere_origin, "reduction_dist: ", reduction_dist
      #print "objcenter",objcoords,"dragpt",self.point_on_sphere
      
      return self.point_on_sphere
      
      
      
      #fulldist=math.sqrt(sum(a^2 for a in objwincoords))
      
      #if fulldist>spheresize+0.0000000000000000001:
        #ratio=(fulldist-spheresize)/fulldist
      #else:
        #ratio=0.0000000000000000001
        
      #new_win_coords=tuple(a*ratio for a in objwincoords)
      
      #new_world_coords=(pickptx,pickpty,pickptz)=GLU.gluUnProject(new_win_coords[0],new_win_coords[1],new_win_coords[2],modelview,projection,viewport)
                
    
    def winMoveToWorldMoveV(self,x,y,z):
      #debug_trace()
      viewport=GL.glGetIntegerv(GL.GL_VIEWPORT)
      modelview=GL.glGetDoublev(GL.GL_MODELVIEW_MATRIX)
      projection=numpy.copy(GL.glGetDoublev(GL.GL_PROJECTION_MATRIX))
      #modelview[3][0]=0
      #modelview[3][1]=0
      #print "sending to gluunproject: "+str((dist,0,self.zTra,modelview,projection,viewport))
      #ztra=self.zTra()
      #ztra=1.00001
      
      #self.poses[self.currframe].z
      #ztra=0.001
      #projection[0,0]=1.0
      #projection[1,1]=1.0 # move the object according to orthogonal projection not perspective - otherwise you get far away objects moving too slow..

#first figure out how much the z should move us by
      (vec1x,vec1y,vec1z)=GLU.gluUnProject(z,0,0,modelview,projection,viewport)
      (vec2x,vec2y,vec2z)=GLU.gluUnProject(0,0,0,modelview,projection,viewport)
      
      zamt=math.sqrt((vec1x-vec2x)**2+(vec1y-vec2y)**2+(vec1z-vec2z)**2)
      if z>0:
        zdir=zamt
      else:
        zdir=-zamt      
      
      #now get the z of the object from cam so that movements are proportional
      (objWinX,objWinY,objWinZ)=GLU.gluProject(self.poses[self.currframe].x,self.poses[self.currframe].y,self.poses[self.currframe].z,modelview,projection,viewport)
      # do unprojection
      (xold,yold,zold)=GLU.gluUnProject(0,0,objWinZ-zdir/2,modelview,projection,viewport)
      (xnew,ynew,znew)=GLU.gluUnProject(x,-y,objWinZ+zdir/2,modelview,projection,viewport)
      #if x2-x1 < 0:
          #return x1-x2
      #else:
          #return x2-x1
      #return x2-x1
      
      
      win_move_multiplier=1
      return tuple(b*win_move_multiplier for b in (xnew-xold,ynew-yold,znew-zold))
        
      
    def winMoveToWorldMove(self,dist):
      viewport=GL.glGetIntegerv(GL.GL_VIEWPORT)
      modelview=GL.glGetDoublev(GL.GL_MODELVIEW_MATRIX)
      projection=numpy.copy(GL.glGetDoublev(GL.GL_PROJECTION_MATRIX))
      #modelview[3][0]=0
      #modelview[3][1]=0
      #print "sending to gluunproject: "+str((dist,0,self.zTra,modelview,projection,viewport))
      #ztra=self.zTra()
      ztra=1.001
      
      ztra=0.001
      projection[0,0]=1.0
      projection[1,1]=1.0 # move the object according to orthogonal projection not perspective - otherwise you get far away objects moving too slow..
      
      
      (x1,y1,z1)=GLU.gluUnProject(0,0,ztra,modelview,projection,viewport)
      (x2,y2,z2)=GLU.gluUnProject(dist,0,ztra,modelview,projection,viewport)
      #if x2-x1 < 0:
          #return x1-x2
      #else:
          #return x2-x1
      #return x2-x1
      return win_move_multiplier*(x2-x1)
  
  
def error_usage():
  print
  print "Usage:"
  print "       label_vid  MODEL_FILE  VIDEO_FILE  INSTRINSIC_CALIBRATION_PARAMS  EXTRINSIC_CALIBRATION_PARAMS  [PREEXISTING_LABELS_FILE]"
  print
  sys.exit(1)

if __name__ == '__main__':
    
    app = QtGui.QApplication(sys.argv)


    try:
      print "Loading model file: "+sys.argv[1]
      modelfile=sys.argv[1]
    except IndexError:
      QtGui.QMessageBox.critical(None, "Video labelling",
            "No model file supplied.")
      error_usage()#sys.exit(1)
    
    try:
      videofile=sys.argv[2]
    except IndexError:
      QtGui.QMessageBox.critical(None, "Video labelling",
          "No video file supplied.")
      error_usage()#sys.exit(1)   
      
    calib_params=None
    if len(sys.argv)>3:
      try:
        calib_int=sys.argv[3]
      except IndexError:
        QtGui.QMessageBox.critical(None, "Video labelling",
              "No intrinsic calibration file supplied.")
        error_usage()#sys.exit(1)        


      try:
        calib_ext=sys.argv[4]
      except IndexError:
        QtGui.QMessageBox.critical(None, "Video labelling",
              "No extrinsic calibration file supplied.")
        error_usage()#sys.exit(1)              
        
      try:
        labels_file=sys.argv[5]
      except IndexError:
        labels_file=None          
        
        
      #try:
      calib_params=calib.CalibFile(calib_int,calib_ext)
      #except Exception as e:
        #QtGui.QMessageBox.critical(None, "Video labelling",
              #"Something wrong with calibration files.")
        #print e
        #sys.exit(1)
    window = Window(modelfile,videofile,calib_params)
    #window = Window(modelfile,videofile)
    window.show()
    if labels_file:
      #if os.path.isfile(labels_file):
      window.glWidget.loadTraj(labels_file)
      #else:
        #pass
    sys.exit(app.exec_())
    
    
    