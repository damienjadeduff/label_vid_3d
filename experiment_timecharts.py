#!/usr/bin/env python
import os
import pose_array_error
from PyQt4 import QtCore, QtGui, QtOpenGL
import sys
import matplotlib.pyplot as plt
import math
import random
import pickle
import time
import numpy

random.seed(111)

class ExperimentSet:
  def __init__(self,version):
    pass
    self.version=version
    assert(type(version)==int)

experiment_details=[]
vidnum2expset=dict()

vidnum2expset["0003"]=0
vidnum2expset["0007"]=0
vidnum2expset["0010"]=0
vidnum2expset["0015"]=0
vidnum2expset["0017"]=0
vidnum2expset["0009"]=0
vidnum2expset["2210"]=0
vidnum2expset["3210"]=0

set1=ExperimentSet(0)
set1.sampledata_template="/home/docean/Documents/MorExtra/LabelVid3D/RPushesData/%%%VID%%%.avi.labelledv2.txt"
set1.experiments_dir="/media/TI30596700A/Documents and Settings/docean/Desktop/MortrackPhysics/MortrackPhysicsProg/experiments/output/choiceexp"
set1.modelfile="/media/TI30596700A/Documents and Settings/docean/Desktop/MortrackPhysics/MortrackPhysicsProg/resources/model/soup.ply"
set1.calib_int="/media/TI30596700A/Documents and Settings/docean/Desktop/MortrackPhysics/MortrackPhysicsData/RustamPushes20100625/RustamPushes20100624Intrinsic.cal"
set1.calib_ext="/media/TI30596700A/Documents and Settings/docean/Desktop/MortrackPhysics/MortrackPhysicsData/RustamPushes20100625/RustamPushes20100624Pose.cal"

vidnum2expset["Set2"]=1


set2=ExperimentSet(1)
set2.sampledata_template="/home/docean/Documents/MorExtra/LabelVid3D/NextSet/%%%VID%%%.avi.labelledv2.txt"
set2.experiments_dir="/media/TI30596700A/Documents and Settings/docean/Desktop/MortrackPhysics/MortrackPhysicsProg/experiments/output/ndat"
set2.modelfile="/home/docean/Documents/MorExtra/new_mortrack_data/model/dogukaradenizcay25posetscaled.ply"
set2.calib_int="/home/docean/Documents/MorExtra/new_mortrack_data/calib/kinect_A00365918852041A.cal"
set2.calib_ext="/home/docean/Documents/MorExtra/new_mortrack_data/Set2Vids/karadenizset2_pose.cal"


vidnum2expset["3000"]=2
vidnum2expset["3001"]=2
vidnum2expset["3002"]=2
vidnum2expset["3003"]=2
vidnum2expset["3004"]=2
vidnum2expset["3005"]=2
vidnum2expset["3006"]=2
vidnum2expset["3007"]=2
vidnum2expset["3008"]=2
vidnum2expset["3009"]=2
vidnum2expset["3010"]=2

set3=ExperimentSet(1)
set3.sampledata_template="/home/docean/Documents/MorExtra/LabelVid3D/NextSet/%%%VID%%%.avi.labelledv2.txt"
set3.experiments_dir="/media/TI30596700A/Documents and Settings/docean/Desktop/MortrackPhysics/MortrackPhysicsProg/experiments/output/ndat"
set3.modelfile="/home/docean/Documents/MorExtra/new_mortrack_data/model/dogukaradenizcay25posetscaled.ply"
set3.calib_int="/home/docean/Documents/MorExtra/new_mortrack_data/calib/kinect_A00365918852041A.cal"
set3.calib_ext="/home/docean/Documents/MorExtra/new_mortrack_data/Batch3/batch3_calibpose.cal"

experiment_details.append(set1)
experiment_details.append(set2)
experiment_details.append(set3)
#sampledata/0003.avi.labelledv2.txt  "/media/TI30596700A/Documents and Settings/docean/Desktop/MortrackPhysics/MortrackPhysicsProg/experiments/output/choiceexp/exp_V0003_100p__1scl__Farw__0.25_ZERO_DIS_RA_1_ZERO_DIS_DF/output_0000_video_0003.avi.txt"

condition_mapping=dict()
condition_mapping["0.25_ZERO_DIS_RA_0.001_ZERO_DIS_SW_0.001_SIM_W_FO_SW"]="Force Switch 0.001 + Rank"
condition_mapping["0.25_ZERO_DIS_RA_1_ZERO_DIS_DF"]="Dispersion + Rank"
condition_mapping["0.25_ZERO_DIS_RA_0.0_ZERO_DIS_SW_0.0_SIM_W_FO_SW"]="Force Switch 0 + Rank"
condition_mapping["1_SIM_W_FO_DF"]="Force"
condition_mapping["0.25_ZERO_DIS_RA_1_SIM_W_FO_DF"]="Force + Rank"
condition_mapping["0.25_ZERO_DIS_RA_0.01_ZERO_DIS_SW_0.01_SIM_W_FO_SW"]="Force Switch 0.01 + Rank"


shortnames_vid=dict()

shortnames_vid["0003.avi"]="0003"
shortnames_vid["0009.avi"]="0009"
shortnames_vid["0007.avi"]="0007"
shortnames_vid["0010.avi"]="0010"
shortnames_vid["0015.avi"]="0015"
shortnames_vid["0017.avi"]="0017"
shortnames_vid["2210.avi"]="2210"
shortnames_vid["3210.avi"]="3210"

shortnames_vid["lipton_pickup_sequence_1.avi"]="Cay1"
shortnames_vid["lipton_pickup_sequence_2.avi"]="Cay2"
shortnames_vid["lipton_pickup_sequence_3.avi"]="Cay3"

shortnames_vid["karadenizset2_1.avi"]="Set2"

shortnames_vid["batch3_drops.avi"]="3000"
shortnames_vid["batch3_fallbehindreveal.avi"]="3001"
shortnames_vid["batch3_fallintofullocclusion.avi"]="3002"
shortnames_vid["batch3_fallintopartialocclusion.avi"]="3003"
shortnames_vid["batch3_fallintopartialocclusion2.avi"]="3004"
shortnames_vid["batch3_fastscoop.avi"]="3005"
shortnames_vid["batch3_occlusionrearfall.avi"]="3006"
shortnames_vid["batch3_ontopofbooksfallocclude.avi"]="3007"
shortnames_vid["batch3_ontopofotherobjectfall.avi"]="3008"
shortnames_vid["batch3_partialocclusionfall.avi"]="3009"
shortnames_vid["batch3_sliding.avi"]="3010"

shortnames_vid["0003"]="0003"
shortnames_vid["0009"]="0009"
shortnames_vid["0007"]="0007"
shortnames_vid["0010"]="0010"
shortnames_vid["0015"]="0015"
shortnames_vid["0017"]="0017"
shortnames_vid["2210"]="2210"
shortnames_vid["3210"]="3210"

shortnames_vid["lipton_pickup_sequence_1"]="Cay1"
shortnames_vid["lipton_pickup_sequence_2"]="Cay2"
shortnames_vid["lipton_pickup_sequence_3"]="Cay3"

shortnames_vid["karadenizset2_1"]="Set2"

shortnames_vid["batch3_drops"]="3000"
shortnames_vid["batch3_fallbehindreveal"]="3001"
shortnames_vid["batch3_fallintofullocclusion"]="3002"
shortnames_vid["batch3_fallintopartialocclusion"]="3003"
shortnames_vid["batch3_fallintopartialocclusion2"]="3004"
shortnames_vid["batch3_fastscoop"]="3005"
shortnames_vid["batch3_occlusionrearfall"]="3006"
shortnames_vid["batch3_ontopofbooksfallocclude"]="3007"
shortnames_vid["batch3_ontopofotherobjectfall"]="3008"
shortnames_vid["batch3_partialocclusionfall"]="3009"
shortnames_vid["batch3_sliding"]="3010"

longnames_vid=dict()
for key,item in shortnames_vid.items():
  if(longnames_vid.has_key(item)):
    if len(longnames_vid[item])>len(key):
    #import pdb;pdb.set_trace()
      longnames_vid[item]=key
  longnames_vid[item]=key

matplotlib_colors=['b','r','m','k','g','w','y','c']

def map_condition(tup,mask,invert=False):
  
  ss=""
  
  which=[]
  for cntr in range(0,max(len(mask),len(tup))):
    if mask[cntr] is not None and mask[cntr]:
      doit=True
    else:
      doit=False
      
    if invert:
      doit = not doit
    which.append(doit)
    
  if which[0]:
    ss=ss+"Vid "+tup[0]+", "
  if which[1]:
    ss=ss+str(tup[1])+" partcles, "
  if which[2]:  
    ss=ss+tup[2]+" clusts, "
  if which[3]:  
    if tup[3]=="Tr":
      ss=ss+"new rwgt, "
    else:
      ss=ss+"old rwgt, "
  if which[4]:
    ss=ss+str(tup[4])+" recs, "
  if which[5]:
    if tup[5] in condition_mapping:
      cond=condition_mapping[tup[5]]
    else:
      cond=tup[5]
    ss=ss+cond
    
  if ss[-1]==" ":ss=ss[:-1]
  if ss[-1]==",":ss=ss[:-1]
    
  return ss
  

#def map_condition(cond):
  #if cond in condition_mapping:
    #return condition_mapping[cond]
  #return cond

def get_data_files(expdir):
  for subdir in os.listdir(expdir):

    subdir_full=os.path.join(expdir,subdir)
    if os.path.isdir(subdir_full):
      for filename in os.listdir(subdir_full):

        filename_full = os.path.join(subdir_full,filename)

        if filename[0:6]=="output":
          
          if filename[-4:] == ".txt":
            if not os.path.isdir(filename_full):

              yield (subdir,filename,filename_full)


class ErrorsByCondition:
  
  def __init__(self,datafiles):
    
    self.datafiles=datafiles

    self.all_vid_num=set()
    self.all_num_particles=set()
    self.all_num_spatial=set()
    self.all_reweight_method=set()
    self.all_num_recursions=set()
    self.all_condition=set()
  
    self.filename_by_condition=dict()
    
    #self.deb1=[]
    #self.deb2=[]
    
    for direc,fname,filename_full in self.datafiles:
      
      tup=self.direc_to_tup(direc)
      vid_num=tup[0]
      num_particles=tup[1]
      num_spatial=tup[2]
      reweight_method=tup[3]
      num_recursions=tup[4]
      condition=tup[5]

      self.all_vid_num.add(vid_num)
      
      self.all_num_particles.add(num_particles)
      
      self.all_num_spatial.add(num_spatial)
      
      self.all_reweight_method.add(reweight_method)
      
      self.all_num_recursions.add(num_recursions)
      
      self.all_condition.add(condition)
      
      #if condition[0]=="_":
        #self.deb1.append(tup)
      #else:
        #self.deb2.append(tup)
      
      if not self.filename_by_condition.has_key(tup):
        self.filename_by_condition[tup]=[]
      self.filename_by_condition[tup].append(filename_full)
      print tup
    #import pdb;pdb.set_trace()

  def direc_to_tup(self,direc):
    
      if direc[0:3]=="exp":
          
      
        vid_num=direc[5:9]
        
        num_particles=int(direc[10:13])
        
        num_spatial=direc[16]
        
        reweight_method=direc[22:24]
        
        num_recursions=1
        
        condition=direc[28:]

      else:
        vid_num=direc[1:5]
        
        num_particles=int(direc[6:9])
        
        num_spatial=direc[12]
        
        
        reweight_method=direc[18:20]
        
        num_recursions=direc[24]
        
        condition=direc[28:]        
        
        
      tup=(vid_num,num_particles,num_spatial,reweight_method,num_recursions,condition)   
      return tup
  
  def getTups(self):
    for tup in self.filename_by_condition.keys():
      yield tup
  
  def getTupsByCond(self,mask):
    newtups=set()
    for tup in self.stepdists_by_condition.keys():
      newlist=[]
      for cntr in range(0,len(tup)):
        if mask[cntr]:
          newlist.append(tup[cntr])
        else:
          newlist.append(False)
      newtup=tuple(newlist)
      if newtup not in newtups:
        newtups.add(newtup)
        yield newtup
      
  def getTupsWithMissing(self,match):
    
    for tup in self.stepdists_by_condition.keys():
      matched=True
      for cntr in range(0,len(tup)):
        if len(match)>cntr and match[cntr] is not None and match[cntr] and tup[cntr]<>match[cntr]:
          matched=False
          break
      if matched:
        yield tup
    
  def calculateStepdists(self,as_generator=False):

    #self.filename_by_condition=dict()
    self.stepdists_by_condition=dict()
    self.stepdists_by_condition_avg=dict()
    self.stepdists_by_condition_stderr=dict()
  
    condition_cntr=0
  
    for vid_num in self.all_vid_num:
      print "vid_num",vid_num
      expno=vidnum2expset[vid_num]
      expdetails=experiment_details[expno]      
      
      labelled_filename=expdetails.sampledata_template.replace("%%%VID%%%",vid_num)
      labelled_filename=labelled_filename.replace("%%%VID%%%",longnames_vid[vid_num])
      
      labels_exist=False
      try:
        with open(labelled_filename): labels_exist=True
      except IOError:
        print 'Could not open labels file',labelled_filename
      #labelled_filename=expdetails.sampledata_template.replace("%%%VID%%%",shortnames_vid[vid_num])
      
      if labels_exist:
        for num_particles in self.all_num_particles:
          print "num_particles",num_particles
          
          for num_spatial in self.all_num_spatial:
            for reweight_method in self.all_reweight_method:
              for num_recursions in self.all_num_recursions:
                for condition in self.all_condition:
                  
                  #print "condition",num_spatial,reweight_method,condition
                  tup=(vid_num,num_particles,num_spatial,reweight_method,num_recursions,condition)   
                  print "tup",tup
                  

                  

                  #import pdb;pdb.set_trace()   
                  if self.filename_by_condition.has_key(tup):
                    for filename in self.filename_by_condition[tup]:
                  
                      stepdists=pose_array_error.array_file_img_error(expdetails.modelfile,expdetails.calib_int,expdetails.calib_ext,labelled_filename,filename,verbose=0)#verbose=0
                      if not self.stepdists_by_condition.has_key(tup):
                        self.stepdists_by_condition[tup]=[]
                      self.stepdists_by_condition[tup].append(stepdists)
                    
                    #now calculate mean
                    num_files=0
                    stepdists_sum=None                    
                    for stepdists in self.stepdists_by_condition[tup]:
                        
                      if stepdists_sum is None:
                        stepdists_sum=stepdists
                      else:
                        stepdists_sum=[a+b for (a,b) in zip(stepdists,stepdists_sum)]
                        
                      num_files=num_files+1
                      
                  #if stepdists_sum is not None:
                    if num_files<=0:import pdb;pdb.set_trace()#unexpected
                    
                    stepdists_avg=[s/num_files for s in stepdists_sum]
                    self.stepdists_by_condition_avg[tup]=stepdists_avg
                    
                    # now calculate stddev
                    
                    stepdists_sumsquareddeviance=None
                    
                    for stepdists in self.stepdists_by_condition[tup]:
                        
                      diffsqdists=[(a-b)**2 for (a,b) in zip(stepdists,stepdists_avg)]  
                        
                      if stepdists_sumsquareddeviance is None:
                        stepdists_sumsquareddeviance=diffsqdists
                      else:
                        stepdists_sumsquareddeviance=[a+b for (a,b) in zip(diffsqdists,stepdists_sumsquareddeviance)]

                      
                  #if stepdists_sum is not None:
                    stepdists_stddev=[math.sqrt(s)/num_files for s in stepdists_sumsquareddeviance]
                    #stepdists_stddev=[s/num_files for s in stepdists_sumsquareddeviance]
                    stepdists_stderr=[s/math.sqrt(num_files) for s in stepdists_stddev]
                    self.stepdists_by_condition_stderr[tup]=stepdists_stderr                    
                    
                    if as_generator:
                      yield (tup,self.stepdists_by_condition[tup],self.stepdists_by_condition_avg[tup],self.stepdists_by_condition_stderr[tup])
                  
                    
    
if __name__ == '__main__':
  
  
  app = QtGui.QApplication(sys.argv)
  
  if len(sys.argv)>1:
    
    data_filename=sys.argv[1]
    with open(data_filename,'r') as datafile:
      data=pickle.load(datafile)
      
  else:
    
    data_filename="allimgerrors_"+str(time.mktime(time.gmtime()))+".pkl"
  
    print "Getting exp file info..."
    fs=[]
    for exp in experiment_details:
      print "Doing next exp..."
      fs=fs+[f for f in get_data_files(exp.experiments_dir)]
    data=ErrorsByCondition(fs)
    
    print "Building error info..."
  
    for garbage in data.calculateStepdists(as_generator=True):
      #for garbage in data.calculateStepdists(as_generator=True):
      print "Saving to file (incremental save)",data_filename,"..."
  
      with open(data_filename,'w') as datafile:
        pickle.dump(data,datafile)
        
    print "Saving to file",data_filename,"..."
  
    with open(data_filename,'w') as datafile:
      pickle.dump(data,datafile)

  # mask represents which concrete factors make up the title
  def tup2title(tup,mask):
    ss=""
    for cntr in range(0,len(mask)):
      if len(mask)>cntr and mask[cntr] is not None and mask[cntr]:
        if cntr>0:
          ss=ss+"_"
        ss=ss+str(tup[cntr])
        cntr=cntr+1
    return ss
  
  # mask represents which concrete factors make up the title
  # so the ones not in the mask are varied over, so the label is based on these varying ones
  def tup2label(tup,mask):
    ss=""
    for cntr in range(0,len(mask)):
      if len(mask)<=cntr or mask[cntr] is None or not mask[cntr]:
        if cntr>0:
          ss=ss+"_"
        ss=ss+str(tup[cntr])
        cntr=cntr+1
    if ss[0]=="_":
      ss=ss[1:]        
    return ss
      
  
  fig_cntr=[0]
  
  def doPlot(mask,show_runs=False,stderr=False,showplot=False):
    

    
    colors=dict()
    
    col_ptr=0
    for search_tup in data.getTupsByCond(mask):
      
      print search_tup
      
      
      fig_cntr[0]=fig_cntr[0]+1
      plt.figure(fig_cntr[0])
      labellist=[]
      plotlist=[]
      
      
      for tup in data.getTupsWithMissing(search_tup):
        
        
        #label=tup2label(tup,mask)
        label=map_condition(tup,mask,invert=True)
        
        
        
        if not colors.has_key(label):
          colors[label]=matplotlib_colors[col_ptr]
          col_ptr=col_ptr+1
          if col_ptr>=len(matplotlib_colors):col_ptr=0
        
        
        plt.plot(range(0,len(data.stepdists_by_condition_avg[tup])),data.stepdists_by_condition_avg[tup],label=label,color=colors[label],linewidth=2)
        
        if show_runs:
          for stepdists in data.stepdists_by_condition[tup]:
            plot=plt.plot(range(0,len(stepdists)),stepdists,color=colors[label],alpha=0.1,linewidth=4)
        
        if stderr:
            lowers=[a-b for (a,b) in zip(data.stepdists_by_condition_avg[tup],data.stepdists_by_condition_stderr[tup])]
            uppers=[a+b for (a,b) in zip(data.stepdists_by_condition_avg[tup],data.stepdists_by_condition_stderr[tup])]
            #import pdb;pdb.set_trace()
            plt.fill_between(range(0,len(lowers)),lowers,uppers,color=colors[label],alpha=0.1)
            #plotlist.append(plot)
            
          
          #plot=plt.plot(numpy.array(data.stepdists_by_condition[tup]).T,label=label)
          
          #labellist.append(label)
          
      plt.ylabel("Image error (pixels)")
      plt.xlabel("Time step (frame no)")
      #title=tup2title(search_tup,mask)
      title=map_condition(search_tup,mask)
      plt.title(title)
      
      plt.legend()
      #plt.legend(plotlist,labellist)
      try:
        os.mkdir("outputfigs")    
      except:
        pass
      fname="outputfigs/"+title.replace(",","").replace(" ","")+".png"
      print "saving plot ",fname
      plt.savefig(fname)
      if showplot:
        plt.show()
  
  
  print "PLOT: See the effect of particle number"
  mask=(True,False,True,True,True,True) 
  doPlot(mask,showplot=False,stderr=True)
        
  print "PLOT: See the effect of reweight to keep close to original dist."
  mask=(True,True,True,False,True,True) 
  doPlot(mask,showplot=False,stderr=True)
  
  print "PLOT: See the effect of num spatial clusts."
  mask=(True,True,False,True,True,True) 
  doPlot(mask,showplot=False,stderr=True)  
  
  print "PLOT: Effect of condition"
  mask=(True,True,True,True,True,False) 
  doPlot(mask,showplot=False,stderr=True)
  
  print "PLOT: Effect of num recursions"
  mask=(True,True,True,True,False,True) 
  doPlot(mask,showplot=False,stderr=True)  